var loader = '<i class="fa fa-spinner fa-spin"></i>Processing...';

$(document).ready(function(){
    $(document).foundation();
    $('input[type="tel"]').mask('999-999-9999');
    $('input[type="number"]').mask('99999.9');
    $('#dialog').on('click', '.btn-close', function(){
        $('#dialog').foundation('close');
    });
    
    $(document).on('click', 'a.no-link', function(e){
        e.preventDefault();
    });
});

function getState(country, val){
    $.get('/signup/state.html', {country: country}, function(html){
       var s = $(document).find('#state');
       s.html(html).val(val);
       if(val !== ''){
           s.prop('disabled', true);
       } else {
           s.prop('disabled', false);
       }
    });
}

function RoundNum(num, length) { 
    var number = Math.round(num * Math.pow(10, length)) / Math.pow(10, length);
    return number;
}