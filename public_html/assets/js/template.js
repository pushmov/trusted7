var dtf, uploadObj;
$(document).ready(function(){
	if ($('#tbl_fields').length > 0) {
		dtf = $('#tbl_fields').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ordering: false,
			ajax: {
				url: '/users/template/fields.json',
				data: function ( d ) {
					d.template_id = template_id;
					d.page_dropdown = $('#page_dropdown').val();
				}
			},
			columnDefs: [
				{'className': 'center', 'targets': [3,4,6,7]},
				{'className': 'placement', 'targets': [1]}
			],
			bFilter: false,
			responsive: true,
			createdRow: function(row, data, dataIndex){
				if(dataIndex === 0) {
					$(row).addClass('row-selected');
				}
			},
			iDisplayLength: 25
		});
		$(document).on({
			mouseenter: function(){
				$(this).addClass('row-highlighted');
			},
			mouseleave: function(){
				$(this).removeClass('row-highlighted');
			}
			}, 'tr');

		$(document).on('click', '.placement-value', function(){
			$('tr').removeClass('row-selected');
			$(this).closest('tr').addClass('row-selected');
		});

	}
	setPageImage();
    $.get('/users/template/placement.json', {id: null, template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
        if (json.error) {
            return false;
        } else {
            setTimeout(function(){
                $(document).find('a.placement-value[data-id="'+json.id+'"]').click();
            },500);
        }
    });
});

$(document).ready(function(){
	$.get('/users/template/upload_page.html', {page: $('#page_dropdown').val(), template: template_id}, function(html){
		$('#upload_page_image_btn').html(html);
	});

	$(document).on('click', '.field-edit', function(){
		$.get('/users/template/edit_field.html', {id: $(this).attr('data-id')}, function(html){
			$('#dialog_medium').html(html);
			$('#dialog_medium').find('.color').colorPicker();
			$('#dialog_medium').foundation('open');
			if ($('#dialog_medium').find('#group').val() > 0) {
				$.get('/users/group/group_fields.html', {group_id: $('#field_content').find('#group').val(), value: $('#dialog_medium').find('#group').attr('data-field')}, function(html){
					$('#content').parent().html(html);
				});
			}
		});
	});

	$('#btn_template_update').on('click', function(){
		var btn = $(this);
		var txt = btn.html();
		var form = $(this).closest('form');
		form.find('.highlight-alert').remove();
		form.find('.validation-error').removeClass('validation-error');
		form.find('.validation-input-error').remove();
		btn.prop('disabled', true);
		btn.html(loader);
		$.post(form.attr('action'), form.serialize(), function(json){
			btn.prop('disabled', false);
			btn.html(txt);
			if (json.errors) {
				$.each(json.errors.message, function(k,v){
					$('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
				});
			} else if(json.success) {
				form.find('#notification').html('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</div>');
			}
		});
	});

	$('#add_page').on('click', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post('/users/template/add_page.json', {template_id: template_id}, function(json){
			btn.prop('disabled', false);
			btn.html(txt);
			if (json.success) {
				dtf.ajax.reload();
				window.location.href = json.success.message;
			}
		});
	});

	$('#add_field').on('click', function(){
		$.get('/users/template/add_field.html', {template_id: template_id}, function(html){
			$('#dialog_medium').html(html);
			$('#dialog_medium').foundation('open');
			$('#dialog_medium').find('.color').colorPicker();
		});
	});

	$('#dialog_medium').on('click', '#btn_save_fields', function(){
		$('.page-image-control').find('.alert-box').remove();
		var btn = $(this);
		var txt = btn.html();
		var form = $(this).closest('form');
		form.find('.validation-error').removeClass('validation-error');
		form.find('.validation-input-error').remove();
		form.find('.highlight-alert').remove();
		btn.prop('disabled', true);
		btn.html(loader);
		var data = form.serializeArray();
		data.push({name : 'data[page]', value: $('#page_dropdown').val()});
		$.post(form.attr('action'), data, function(json){
			btn.prop('disabled', false);
			btn.html(txt);
			if (json.errors) {
				$.each(json.errors.message, function(k,v){
					$('#dialog_medium').find('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
				});
			} else if(json.success){
				dtf.ajax.reload();
				if(ft !== null) {
					ft.unplug();
					rect.remove();
				}
				$('#save_placement').attr('data-id', json.id);
				$('#dialog_medium').foundation('close');
                setTimeout(function(){
                    $(document).find('a.placement-value[data-id="'+json.id+'"]').click();
                },500);
			}
		});
	});

	$(document).on('click', '.template-delete', function(){
		$.get('/users/template/delete.html', {id: $(this).attr('data-id')}, function(html){
			$('#dialog').html(html);
			$('#dialog').foundation('open');
			$('#dialog').on('click', '.btn-submit', function(){
				var btn = $(this);
				var txt = btn.html();
				btn.prop('disabled', true);
				btn.html(loader);
				var form = $(this).closest('form');
				$.post(form.attr('action'), form.serialize(), function(json){
					btn.prop('disabled', false);
					btn.html(txt);
					dtf.ajax.reload();
					if(ft !== null) {
						ft.unplug();
						rect.remove();
					}
                    $.get('/users/template/placement.json', {id: null, template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
                        if (json.error) {
                            return false;
                        } else {
                            setTimeout(function(){
                                $(document).find('a.placement-value[data-id="'+json.id+'"]').click();
                            },500);
                        }
                    });
					$('#dialog').foundation('close');
				});
			});
		});

	});

	$('#dialog_medium').on('change', '#type_id', function(){
		$.get('/users/template/field_content.html', {type_id: $(this).val()}, function(html){
			$('#field_content').html(html);
			if ($('#field_content').find('#group').val() > 0) {
				$.get('/users/group/group_fields.html', {group_id: $('#field_content').find('#group').val(), value: null}, function(html){
					$('#content').parent().html(html);
				});
			}
		});
	});

	$('#size').on('change', function(){
		$.get('/users/template/size.json', {id: $(this).val(), template_id: template_id}, function(json){
			$('#width').val(json.width);
			$('#height').val(json.height);
            if(json.editable) {
                $('.sizes').removeAttr('readonly');
            } else {
                $('.sizes').attr('readonly', 'readonly');
            }
		});
	});

	$('#delete_page').on('click', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		$.get('/users/template/delete_page.html', function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			$('#dialog').html(html);
			$('#dialog').foundation('open');
			$('#dialog').on('click', '#btn_delete_page', function(){
				var btn = $(this);
				var txt = btn.html();
				var form = $(this).closest('form');
				btn.prop('disabled', true);
				btn.html(loader);
				$.post(form.attr('action'), {template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
					btn.prop('disabled', false);
					btn.html(txt);
					window.location.reload();
				});
			});
		});
	});

	$.get('/users/template/is_custom.json', {size: $('#size').val()}, function(json){
		if(json.readonly) {
			$('.sizes').attr('readonly', 'readonly');
		} else {
            $('.sizes').removeAttr('readonly');
		}
		$('#size').val(json.value);
	});

	$('#dialog_medium').on('change', '#group', function(){
		$.get('/users/group/group_fields.html', {group_id: $(this).val(), value: null}, function(html){
			$('#content').parent().html(html);
		});
	});

	$('#btn_template_preview').on('click', function(){
		window.open(baseUrl + 'render/pdf/' + $('#id').val(), 'preview', 'target=_blank');
	});

    $('#btn_page_preview').on('click', function(){
        window.open(baseUrl + 'render/pdf/' + $('#id').val() + '/'+$('#page_dropdown').val(), 'preview', 'target=_blank');
    });
});