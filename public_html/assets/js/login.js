$(document).ready(function(){
    $('#login').on('click', function(){
        submitForm($(this).closest('form'));
    });
    $('.form-login #passwrd').on('keyup', function(e){
        if(e.which === 13){
            submitForm($('.form-login'));
        }
    });
    $('#reset').on('click', function(){
        resetForm($(this));
    });
});

function submitForm(form){
    form.find('.alert-box').remove();
    $.post(form.attr('action'), form.serialize(), function(json){
        if(json.error) {
            $.each(json.error, function(k, m){
                form.find('.btn-primary')
                .after('<div data-alert class="alert-box warning info radius">'+m+'</div>');
            });
        } else if(json.success) {
            window.location.href = '/users/dashboard';
        }
    });
}

function resetForm(btn) {
    var txt = btn.html();
    btn.html(loader);
    btn.prop('disabled', true);
    var form = $('#form_reset');
    form.find('.highlight-alert').remove();
    $.post(form.attr('action'), form.serialize(), function(json){
        btn.html(txt);
        btn.prop('disabled', false);
        if(json.success) {
            $('#reset').before('<div class="highlight-alert"><a href="#" class="highlight tos">'+json.success.message+'</a></div>');
            form.find('input').val('');
        } else if(json.error) {
            $('#reset').before('<div class="highlight-alert"><a href="#" class="highlight red">'+json.error.message+'</a></div>');
        }
    });
}