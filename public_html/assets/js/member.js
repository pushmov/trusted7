var dtConfig = {
    processing: true,
    serverSide: true,
    stateSave: true,
    aoColumnDefs : [
        { 'bSortable': false, 'aTargets': [ -1 ] } 
    ]
};
$(document).ready(function(){
    if($('.my-team').find('#my_team').length > 0){
        dtConfig.ajax = { url: '/users/dashboard/lists.json' };
        var dt = $('.my-team').find('#my_team').DataTable(dtConfig);
    }

    if($('#my_client').length > 0) {
        dtConfig.ajax = { url: '/users/client/lists.json' };
        var dtc = $('#my_client').DataTable(dtConfig);
    }

    if($('#my_templates').length > 0) {
        dtConfig.ajax = { url: '/users/template/lists.json' };
        dtConfig.aoColumnDefs = [{ 'bSortable': false, 'aTargets': [ -1, -3,-4 ] } ];
        var dtt = $('#my_templates').DataTable(dtConfig);
    }
    
    if($('#my_group').length > 0) {
        dtConfig.ajax = { url: '/users/group/lists.json' };
        var dtg = $('#my_group').DataTable(dtConfig);
    }

    $('.btn-add-client').on('click', function(e){
        $.get('/users/client/add_client.html', function(html){
            $('#dialog_large').html(html);
            $('#dialog_large').foundation('open');
        });
    });

    $('#dialog_large').on('click', '#btn_add_client', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('input').removeClass('validation-error');
        form.find('.validation-input-error').remove();
        $.post(form.attr('action'), form.serialize(), function(json){
            btn.html(txt);
            btn.prop('disabled', false);
            if(json.error) {
                $.each(json.error.message, function(k,v){
                    $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                });
            } else if(json.success) {
                form.find('#form_error').html('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</a></div>');
                form.find('input, select').val('');
                dtc.ajax.reload();
            }
        });
    });

    $('#btn_add_user').on('click', function(){
        $.get('/users/team/add_user.html', function(html){
            $('#dialog_large').html(html);
            $('#dialog_large').foundation('open');
        });
    });

    $('#dialog_large').on('click', '#btn_invite_user',function(){
        var form = $(this).closest('form');
        form.find('input').removeClass('validation-error');
        form.find('.validation-input-error').remove();
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        $.post(form.attr('action'), form.serialize(), function(json){
            btn.html(txt);
            btn.prop('disabled', false);
            if(json.error) {
                $.each(json.error.message, function(k,v){
                    $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                });
            } else if (json.success){
                form.find('input, select').val('');
                form.find('#error_notification').html('<a href="#" class="highlight success">'+json.success.message+'</a>');
                form.find('#btn_invite_user').removeAttr('id').prop('data-close', true).html('Close');
                $('#dialog_large').on('click', 'button', function(){
                    $('#dialog_large').foundation('close');
                    $.get('/users/team/load_team_table.html', function(html){
                        $('.my-team').html('');
                        $('.my-team').html(html);
                        dtConfig.ajax = { url: '/users/dashboard/lists.json' };
                        var f = $('.my-team').find('#my_team').DataTable(dtConfig);
                        f.ajax.reload();
                    });
                });
            }
        });
    });

    $(document).on('click', '.client-remove', function(){
        $.get('/users/client/confirm_delete.html', {id: $(this).attr('data-id')}, function(html){
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '.btn-submit', function(){
                var form = $(this).closest('form');
                $.post(form.attr('action'), form.serialize(), function(json){
                    $('#dialog').foundation('close');
                	dtc.ajax.reload();
                });
            });
        });

    });

    $(document).on('click', '.btn-user-remove', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        $.get('/users/team/confirm_delete.html', {id: $(this).attr('data-id')}, function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '.btn-submit', function(){
                var btn = $(this);
                var txt = btn.html();
                btn.prop('disabled', true);
                var form = $(this).closest('form');
                $.post(form.attr('action'), form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    $('#dialog').foundation('close');
                    if(parseInt(json.count) > 0) {
                        $.get('/users/team/load_team_table.html', function(html){
                            $('.my-team').html('');
                            $('.my-team').html(html);
                            dtConfig.ajax = { url: '/users/dashboard/lists.json' };
                            var f = $('.my-team').find('#my_team').DataTable(dtConfig);
                            f.ajax.reload();
                        });
                    } else {
                        $('.my-team').html('');
                    }
                    
                });
            });
        });
    });


    $(document).on('click', '.client-email', function(){
        $.get('/users/client/send_message.html', {id: $(this).attr('data-id')} , function(html){
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_send_email_client', function(){
                var btn = $(this);
                var txt = btn.html();
                btn.html(loader);
                btn.prop('disabled', true);
                var form = $(this).closest('form');
                form.find('input').removeClass('validation-error');
                form.find('.validation-input-error').remove();
                $.post(form.attr('action'), form.serialize(), function(json){
                    btn.html(txt);
                    btn.prop('disabled', false);
                    if(json.error) {
                        $.each(json.error.message, function(k,v){
                            $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                        });
                    } else if(json.success) {
                        form.find('input, select, textarea').val('');
                        form.find('#btn_send_email_client').after('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</a></div>');
                        form.find('#btn_send_email_client').hide();
                    }
                });
            });
        });
    });

    $('#create_new_template').on('click', function(){
        $.get('/users/template/add.html', function(html){
            $('#dialog').html(html);
            $('#dialog').foundation('open');

            $('#dialog').on('click', '#btn_create_template', function(){
                var form = $(this).closest('form');
                var btn = $(this);
                var txt = btn.html();
                btn.prop('disabled', true);
                btn.html(loader);
                form.find('input').removeClass('validation-error');
                form.find('.validation-input-error').remove();
                $.post(form.attr('action'), form.serialize(), function(json){
                    btn.html(txt);
                    btn.prop('disabled', false);
                    if(json.errors) {
                        $.each(json.errors.message, function(k, v){
                            $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                        });
                    } else if(json.success) {
                        window.location.href = json.success.location;
                    }
                });
            });
        });
    });


    $(document).on('click', '.client-profile', function(){
        window.location = '/users/client/profile/'+$(this).attr('data-id');
    });

    $(document).on('click', '.user-profile', function(){
        window.location = '/users/team/profile/'+$(this).attr('data-id');
    });

    $(document).on('click', '.btn-template-edit', function(){
        window.location = '/users/template/edit/'+$(this).attr('data-id');
    });

    $('#dialog_large').on('change', '#country', function(){
        getState($(this).val(), $('#dialog_large').find('#state').val());
    });
    
    $('#dialog').on('change', '#size', function(){
        $.get('/users/template/size.json', {id: $(this).val()}, function(json){
            $('#dialog').find('#width').val(json.width);
            $('#dialog').find('#height').val(json.height);
            if(json.editable) {
                $('#dialog').find('.sizes').removeAttr('readonly');
            } else {
                $('#dialog').find('.sizes').attr('readonly', 'readonly');
            }
        });
        
    });
    
    $('#btn_add_group').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/users/group/add.html', function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_add_group', function(){
                var btn = $(this);
                var txt = btn.html();
                var form = $(this).closest('form');
                form.find('.validation-error').removeClass('validation-error');
                form.find('.validation-input-error').remove();
                btn.prop('disabled', true);
                btn.html(loader);
                $.post(form.attr('action'), form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.error) {
                        $.each(json.error.message, function(k, v){
                            $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                        });
                    } else if(json.success) {
                        window.location.href = json.success.message;
                    }
                });
            });
        });
    });
    
    $(document).on('click', '.btn-group-edit', function(){
        window.location.href = '/users/group/edit/'+$(this).attr('data-id');
    });
    
    $(document).on('click', '.btn-group-remove', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/users/group/delete.html', {id: $(this).attr('data-id')}, function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_delete_group', function(){
                var btn = $(this);
                var txt = btn.html();
                var form = $(this).closest('form');
                btn.prop('disabled', true);
                btn.html(loader);
                $.post(form.attr('action'), form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.success) {
                        dtg.ajax.reload();
                    }
                    $('#dialog').foundation('close');
                });
                
            });
        });
    });
});