$(document).ready(function() {
    if($('#file_upload').length > 0) {
        $('#file_upload').uploadFile({
            url: '/users/profile/doupload.json',
            dragDrop: false,
            fileName: 'myfile',
            allowedTypes: 'jpg,jpeg,png',
            returnType: 'json',
            onSuccess: function(files,data,xhr) {
                $('#img_file').val(data[0]);
                loadPhoto();
            }
        });
    }

    loadPhoto();

    $('#btn_update').on('click', function(e){
        var btn = $(this);
        var txt = btn.html();
        var form = $(this).closest('form');
        btn.html(loader);
        btn.prop('disabled', true);
        form.find('input').removeClass('validation-error');
        form.find('.validation-input-error').remove();
        form.find('.highlight-alert').remove();
        $.post('/users/profile/doupdate.json', form.serialize(), function(json){
            btn.html(txt);
            btn.prop('disabled', false);
            if (json.error) {
                $.each(json.error.message, function(i, j){
                    $.each(j, function(k,v){
                        $('#' + i + '_' + k ).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                    });
                });
            } else if(json.success) {
                btn.after('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</a></div>');
            }
        });
    });

    $('#change_password').on('click', function(){
        $.get('/users/profile/change_password.html', {force:force_passwrd}, function(html){
            $('#dialog').html(html);
            $('#dialog').foundation('open');
        });
    });

    $('#dialog').on('click', '#btn_update', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('input').removeClass('validation-error');
        form.find('.validation-input-error').remove();
        $.post(form.attr('action'), form.serialize(), function(json){
            btn.html(txt);
            btn.prop('disabled', false);
            if(json.error) {
                $.each(json.error.message, function(k,v){
                    $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                });
            } else if(json.success){
                btn.before('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</a></div>');
                btn.before('<button type="button" class="button btn-primary"  data-close>Close</button>');
                btn.remove();
                force_passwrd = false;
            }
        });
    });

    if (force_passwrd) {
    	$('#change_password').click();
	}
});

function loadPhoto(){
    $.get('/users/profile/profile_picture.html', function(html){
        $('#current_photo').html(html);
    });
}