$(document).ready(function(){
    $('#file_upload').uploadFile({
		url: baseUrl + 'testdrive/doupload.json',
		dragDrop: false,
		fileName: 'myfile',
		allowedTypes: 'jpg,jpeg,png',
		returnType: 'json',
		onSuccess: function(files,data,xhr) {
			$('#img_file').val(data[0]);
		}
	});
	$("#testsubmit").click(function(e) {
		$('.callout').remove();
		$('#testsubmit').hide();
		$.post(baseUrl + 'testdrive/send.json', $('#testform').serialize(), function(json) {
			if (json.status == 'OK') {
				$('#testsubmit').after(json.message);
			} else {
				$.each(json.errors, function(k,v) {
					$('#' + k).after('<div class="callout alert small">' + v + '</div>');
				});
			}
			$(document).foundation('alert', 'reflow');
		});
		return false;
	});
});
