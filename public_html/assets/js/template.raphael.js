var C = 11.811; //mm to px;
var w;
var h;
if(USER_UNIT === 'inch') {
    w = RoundNum(($('#width').val() / MM_IN), 1) * C;
    h = RoundNum(($('#height').val() / MM_IN), 1) * C;
} else if (USER_UNIT === 'mm') {
    w = $('#width').val() * C;
    h = $('#height').val() * C;
}
var paper = Raphael(document.getElementById('panel'), w, h);
var freeTransformConfig = { 
    drag: ['self', 'center'],
    rotate: false,
    draw: ['bbox'],
    attrs: {
        fill: '#ff0000',
        stroke: '#000'
    },
    size: 25
};
paper.setViewBox(0,0,w,h,true);
paper.setSize('100%', '100%');

var rect = null, ft = null;
function setPageImage(){
    $.get('/users/template/image.json', {template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
        paper.clear();
        if(json.error) {
            paper.image('', 0, 0, '100%', '100%');
        } else if(json.success) {
            var ratio = 0;
            var width = parseInt(json.attrs.width);
            var height = parseInt(json.attrs.height);
            var x = 0;
            var y = 0;
            if (json.attrs.placeholder) {
               x = (w / 2) - (width / 2);
               y = (h / 2) - (height / 2);
            }
            if(width > w) {
                ratio = w / width;
                paper.image('/' + json.image, x, y, w, height * ratio);
                height = height * ratio;
                width = width * ratio;
            } else {
                paper.image('/' + json.image, x, y, width, height);
            }
            
            if(height > h) {
                ratio = h / height;
                paper.image('/' + json.image, x, y, width * ratio, h);
                width = width * ratio;
                height = height * ratio;
            } else {
                paper.image('/' + json.image, x, y, width, height);
            }
        }
    });
}

function setObject(id){
    $.get('/users/template/placement.json', {id: id, template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
        if (json.error) {
            return false;
        } else {
            if(USER_UNIT === 'mm') {
                var x = parseFloat(json.pos_x) * C;
                var y = parseFloat(json.pos_y) * C;
                var w = parseFloat(json.width) * C;
                var h = parseFloat(json.height) * C;
            } else if(USER_UNIT === 'inch') {
                var x = RoundNum((parseFloat(json.pos_x) / MM_IN),1) * C;
                var y = RoundNum((parseFloat(json.pos_y) / MM_IN),1) * C;
                var w = RoundNum((parseFloat(json.width) / MM_IN),1) * C;
                var h = RoundNum((parseFloat(json.height) / MM_IN),1) * C;
            }
            if(json.shape_id === '0') {
                rect = paper.rect(x, y, w, h)
                    .attr('fill', 'rgba(0, 0, 255, 0.28)');
                freeTransformConfig.keepRatio = false;
                freeTransformConfig.scale = ['bboxCorners', 'bboxSides'];
            } else if(json.shape_id === '1') {
                var r = h / 2;
                
                rect = paper.circle((x + r),(y + r),r)
                    .attr('fill', 'rgba(0, 0, 255, 0.28)');
                freeTransformConfig.keepRatio = true;
                freeTransformConfig.scale = ['bboxCorners'];
            }
            ft = paper.freeTransform(rect);
            ft.hideHandles();
            ft.showHandles();
            ft.attrs.rotate = 0;
            ft.apply();
            ft.unplug();
            ft = paper.freeTransform(rect, freeTransformConfig, function(ft, events) {
    //            console.log(ft.attrs);
            });
            rect.toFront();
            
        }
    });
}

$('#save_placement').on('click', function(){
    $('.page-image-control').find('.alert-box').remove();
    var x,y,w,h;
    
    
    if(rect.node.tagName === 'circle') { 
        var r = pos.width / 2;
        var pos = rect.getBBox();
        if(USER_UNIT === 'inch') {
            w = RoundNum(((r * 2) * MM_IN),2) / C;
            h = RoundNum(((r * 2) * MM_IN),2) / C;
            x = RoundNum((pos.cx - r) * MM_IN,2) / C;
            y = RoundNum((pos.cy - r) * MM_IN,2) / C;
        } else {
            w = (r * 2) / C;
            h = (r * 2) / C;
            x = (pos.cx - r) / C;
            y = (pos.cy - r) / C;
        }
    } else if(rect.node.tagName === 'rect') {
        var pos = rect.getBBox();
        if(USER_UNIT === 'inch') {
            w = RoundNum(pos.width * MM_IN,2) / C;
            h = RoundNum(pos.height * MM_IN,2) / C;
            x = RoundNum(pos.x * MM_IN,2) / C;
            y = RoundNum(pos.y * MM_IN,2) / C;
        } else {
            w = pos.width / C;
            h = pos.height / C;
            x = pos.x / C;
            y = pos.y / C;
        }
    }
    var data = {
        pos_x: RoundNum(x, 2),
        pos_y: RoundNum(y, 2),
        width: RoundNum(w, 2),
        height: RoundNum(h, 2),
        template_id : template_id,
        page_id : $('#page_dropdown').val(),
        id: $(this).attr('data-id'),
        rotate: 0
    };
    var btn = $(this);
    var txt = btn.html();
    btn.prop('disabled', true);
    btn.html(loader);
    $.post('/users/template/placement.json', data, function(json){
        btn.prop('disabled', false);
        btn.html(txt);
        if(json.error) {
            var m = '<div data-alert class="alert-box danger">';
            m += '<ul class="unstyled">';
            c = 'danger';
            
            if(json.error.message.pos_x && json.error.message.pos_y) {
                json.error.message.pos_x = 'Minimum x and y values are 5.0';
                delete json.error.message.pos_y;
            }
            if(json.error.message.width && json.error.message.height) {
                delete json.error.message.width;
            }
            $.each(json.error.message, function(k, v){
                m += '<li>' + v + '</li>';
            });
            m += '</ul>';
            m += '</div>';
        }
        else if (json.success) {
            var m = '<div data-alert class="alert-box success">';
            m += '<ul class="unstyled">';
            m += '<li>'+json.success.message+'</li>';
            m += '</ul>';
            m += '</div>';
        }
        if (ft !== null) {
            rect.remove();
            ft.unplug();
        }
        
        dtf.ajax.reload();
        setTimeout(function(){
            $(document).find('tr').removeClass('row-selected');
            $(document).find('.placement-value[data-id="'+json.id+'"]').closest('tr').addClass('row-selected');
        }, 1500);
        setTimeout(function(){
            $(document).find('a.placement-value[data-id="'+json.id+'"]').click();
        },500);
        $('.page-image-control').html(m);
    });
});

$(document).on('click', '.placement-value', function(){
    if(ft !== null) {
        rect.remove();
        ft.unplug();
    }
    setObject($(this).attr('data-id'));
    $('#save_placement').attr('data-id',$(this).attr('data-id'));
});

$('#page_dropdown').on('change', function(){
    if(ft !== null) {
        rect.remove();
        ft.unplug();
    }
    setPageImage();
    $.get('/users/template/placement.json', {id: null, template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
        if (json.error) {
            return false;
        } else {
            setTimeout(function(){
                $(document).find('a.placement-value[data-id="'+json.id+'"]').click();
            },500);
        }
    });
    dtf.ajax.reload();
    $.get('/users/template/upload_page.html', {page: $(this).val(), template: template_id}, function(html){
        $('#upload_page_image_btn').html(html);
    });
});