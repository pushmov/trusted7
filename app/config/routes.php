<?php
return array(
	'_root_' 	=> 'testdrive/index',  // The default route
	'_404_' 	=> 'testdrive/404',    // The main 404 route
	'admin' 	=> 'admin/dashboard',
	'logout' => 'login/logout',
	'activate/:token' => 'users/activate',
	'reset'		=> 'login/forgot',
	'recover/:token'			=> 'login/recover',
	'invite/:token'				=> 'users/invite',
	'team/profile/index/:id'	=> 'team/profile/index',
	'users/client/profile/:id'	=> 'users/client/profile',
	'users/team/profile/:id'	=> 'users/team/profile',
	'users/template/edit/:id'	=> 'users/template/index',
	'users/template/'			=> 'users/template/index',
	'users/group/edit/:id'		=> 'users/group/index',
	'users/group'				=> 'users/group/index',
	'render/pdf/:token/:page'	=> 'render/pdf/',
	'render/pdf/:token'			=> 'render/pdf/',
	'render/pdfdownload/:token/:page'	=> 'render/pdfdownload/',
	'render/pdfdownload/:token'			=> 'render/pdfdownload/',
	
	//'admin/(:any)' => 'admin/$1',
);
