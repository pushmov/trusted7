<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
		<script>var baseUrl = '<?=Uri::base(false);?>';</script>
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/modernizr.js"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
		<script src="/assets/js/jquery.mask.js" type="text/javascript"></script>
		<script src="/assets/js/app.js" type="text/javascript"></script>
		<?php echo \Asset::js($scripts, array(), null, false); ?>
	</head>
	<body>
		<header>
			<h1>header</h1>
		</header>

		<div class="main-content">
			<?=$content;?>
		</div>
		<footer>
			<h1>footer</h1>
		</footer>
	</body>
</html>

<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Test Drive | Welcome</title>
<link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
</head>
<body>

<div class="top-bar">
<div class="top-bar-left">
<ul class="menu">
<li class="menu-text">Yeti Agency</li>
<li><a href="#">One</a></li>
<li><a href="#">Two</a></li>
</ul>
</div>
<div class="top-bar-right">
<ul class="menu">
<li><a href="#">Three</a></li>
<li><a href="#">Four</a></li>
<li><a href="#">Five</a></li>
<li><a href="#">Six</a></li>
</ul>
</div>
</div>

<div class="callout large">
<div class="row column text-center">
<h1>Changing the World Through Design</h1>
<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.</p>
<a href="#" class="button large">Learn More</a>
<a href="#" class="button large hollow">Learn Less</a>
</div>
</div>
<div class="row">
<div class="medium-6 columns medium-push-6">
<img class="thumbnail" src="http://placehold.it/750x350">
</div>
<div class="medium-6 columns medium-pull-6">
<h2>Our Agency, our selves.</h2>
<p>Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna diam porttitor mauris, quis sollicitudin sapien justo in libero. Vestibulum mollis mauris enim. Morbi euismod magna ac lorem rutrum elementum. Donec viverra auctor.</p>
</div>
</div>
<div class="row">
<div class="medium-4 columns">
<h3>Photoshop</h3>
<p>Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna.</p>
</div>
<div class="medium-4 columns">
<h3>Javascript</h3>
<p>Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna.</p>
</div>
<div class="medium-4 columns">
<h3>Marketing</h3>
<p>Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam eu diam mattis viverra. Nulla fringilla, orci ac euismod semper, magna.</p>
</div>
</div>
<hr>
<div class="row column">
<ul class="vertical medium-horizontal menu expanded text-center">
<li><a href="#"><div class="stat">28</div><span>Websites</span></a></li>
<li><a href="#"><div class="stat">43</div><span>Apps</span></a></li>
<li><a href="#"><div class="stat">95</div><span>Ads</span></a></li>
<li><a href="#"><div class="stat">59</div><span>Cakes</span></a></li>
<li><a href="#"><div class="stat">18</div><span>Logos</span></a></li>
</ul>
</div>
<hr>
<div class="row column">
<h3>Our Recent Work</h3>
</div>
<div class="row medium-up-3 large-up-4">
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
<div class="column">
<img class="thumbnail" src="http://placehold.it/550x550">
</div>
</div>
<hr>
<div class="row column">
<ul class="menu">
<li><a href="#">One</a></li>
<li><a href="#">Two</a></li>
<li><a href="#">Three</a></li>
<li><a href="#">Four</a></li>
</ul>
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>


<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Test Drive</title>
</head>
<body>
<div class="con-form">
	<h2>Test Drive</h2>
	<p>* All fields are required.</p>
	<?=$form_msg;?>
	<form action="<?=$form_action;?>" method="post" enctype="multipart/form-data" name="testform" id="testform">
		<label for="first_name">First Name: </label>
		<input type="text" id="first_name" name="data[first_name]" value="<?=$data['first_name'];?>" />
		<label for="last_name">Last Name: </label>
		<input type="text" id="last_name" name="data[last_name]" value="<?=$data['last_name'];?>" />
		<label for="company">Company: </label>
		<input name="data[company]" type="text" id="company" size="40" value="<?=$data['company'];?>" />
		<label for="phone_cell">Cell Phone: </label>
		<input type="text" id="phone_cell" name="data[phone_cell]" value="<?=$data['phone_cell'];?>" />
		<label for="email">Email: </label>
		<input name="data[email]" type="text" id="email" size="40" value="<?=$data['email'];?>" />
		<label for="website">Website: </label>
		<input name="data[website]" type="text" id="website" size="40" value="<?=$data['website'];?>" />
		<label for="client">Client's Name: </label>
		<input name="data[client]" type="text" id="client" value="<?=$data['client'];?>" />
		<label for="address">Property Address: </label>
		<input name="data[address]" type="text" id="address" size="40" value="<?=$data['address'];?>" />
		<label for="city">Property City: </label>
		<input name="data[city]" type="text" id="city" value="<?=$data['city'];?>" />
		<label for="photo">Property Photo: </label>
		<input name="photo" type="file" id="photo">
		<br class="clear">
		<input type="submit" value="Submit Now" id="testsubmit" name="testsubmit">
		<br class="clear">
	</form>
</div>
</body>
</html>
