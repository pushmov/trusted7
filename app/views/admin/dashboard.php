<div class="small-12 column">
<h2>Template Fields</h2>
</div>
<form action="<?=\Uri::create('admin')?>" method="post" id="data_form">
<div class="small-12 column" id="main_div">
<div class="small-7 column"   style="margin-left: -14px;">
	    <button id="btn_logout" class="button small fl">Logout</button>
	    <input type="text" name="template_name" class="fl" style="width:48%;margin-right:10px;"/>
	    <button id="" class="button small">Template Name</button>
	    <button id="" class="button small">Create New Template</button>
		<div class="filter small-12" >	
				<div>
		        	
		        	<button id="" class="button small">Template Id Number</button> 
		        	<button id="" class="button small">Template Name</button>
		        </div>
			 
                		        
				<?=\Form::select('id_no_template', $id_template, $id_templates, array('id' => 'id_no_template'));?>
				<?=\Form::select('id_template', $id_template, $cbo_templates, array('id' => 'id_template'));?>  Page: <?=\Form::select('page', $page, $cbo_pages, array('id' => 'page'));?> Page Image: <span id="fileupload" class="button">Select File To Upload</span>				
		</div>	
   
	<button id="btn_load" type="button" class="button small fl">Load</button>
	<button  type="button" id="btn_save" class="button small fl">Save</button> 
	 <button id="btn_addpage" type="button" class="button small fl">Add Page</button>
	 <button type="button" id="btn_addfield" class="button small fl">Add Field</button>
	  <button type="button" id="btn_preview" class="button small fl">Full Page Preview</button> 
	   <button  type="button" id="btn_refresh" class="button small fl">Refresh</button>
	    <select name="feild_ids"  id="fields_id" class="small-2 select_class">
	    	<option value=""> Select </option>
	    </select> 
	    	<button id="goField" type="button" class="button small fl">Go to field</button> 
</div>
 <div class="small-5 column " id="iframe" style="float:right">
    <iframe src="<?=\Uri::create('admin')?>/templates/dopreview/<?= $id_template; ?>/<?= $page; ?>/" width="530" height="600"></iframe>
  </div>
	
		<div class="datalist">
		  	<?php				
			//$result = array();
			function Get_fieldsValue($type){
				$sql = 'SELECT * FROM `fields_data` WHERE `field_type` ="'.$type.'" ';
				$result = DB::query($sql, DB::SELECT)->execute();
				$data = $result->as_array();
				$field_values = explode(",",$data[0]['field_values']);
				
				$options = '';
				foreach($field_values as $filed){
					//print_r($filed);
					$options .='<option value = "'.$filed.'">'.$filed.'</option>';
				}
				return $options;
			}

			/*echo "<pre>(((";
			print_r($fields);
			echo ")))</pre>";*/
			?>
				<table id="datalist" class="simple-edit" style="float:left">
				<thead>
					<tr>
						<th>Field</th>
						<th>Field Name</th>
						<th>Location</th>
						<th>X Coord</th>
						<th>Y Coord</th>
						<th>Width</th>
						<th>Height</th>
						<th>Font</th>
						<th>Font Size</th>
						<th>Font Style</th>
						<th>Font Color</th>
						<th>Text Align</th>
						<th>Text Content123</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($fields as $field): ?>
						<tr id="id_<?=$field['id'];?>">
							<td><input name="data[f_<?=$field['id'];?>][id_field]" type="text" value="<?=$field['id_field'];?>" size="5" /></td>
						<?php  ?>
							<td><select name="data[f_<?=$field['id'];?>][field_name]"><option value="Name 1" <?php /* if($field['field_name']=='Name 1'){ echo 'selected'; } */ ?>> Name 1 </option><option value="Name 2" <?php /* if($field['field_name']==1.0){ echo 'selected'; } */ ?>> Name 2 </option>
								<option value="Name 3" <?php /* if($field['field_name']=='Name 3'){ echo 'selected'; } */ ?>> Name 3 </option>
							</select></td>
						<?php  ?>
							<td><input name="data[f_<?=$field['id'];?>][location]" type="text" value="<?=$field['location'];?>" size="2" /></td>
							<td><input name="data[f_<?=$field['id'];?>][pos_x]" type="text" value="<?=$field['pos_x'];?>" size="5" /></td>
							<td><input name="data[f_<?=$field['id'];?>][pos_y]" type="text" value="<?=$field['pos_y'];?>" size="5" /></td>
							<td><select name="data[f_<?=$field['id'];?>][width]">
								<option value="1.0" <?php if($field['width']==1.0){ echo 'selected'; } ?>> 1.0 </option><option value="2.0" <?php if($field['width']==2.0){ echo 'selected'; } ?>> 2.0 </option>
								<option value="3.0" <?php if($field['width']==3.0){ echo 'selected'; } ?>> 3.0 </option></select></td>
							<td>
								<select name="data[f_<?=$field['id'];?>][height]">
									<option value="1.0" <?php if($field['height']==1.0){ echo 'selected'; } ?>> 1.0 </option><option value="2.0" <?php if($field['height']==2.0){ echo 'selected'; } ?>> 2.0 </option>
									<option value="3.0" <?php if($field['height']==3.0){ echo 'selected'; } ?>> 3.0 </option>
								</select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_id]">
								<option value="100" <?php if($field['font_id']=='100'){ echo 'selected'; } ?>> 100 </option>
								<option value="200" <?php if($field['font_id']=='200'){ echo 'selected'; } ?>> 200 </option>
								<option value="300" <?php if($field['font_id']=='300'){ echo 'selected'; } ?>> 300 </option>
							</select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_size]">
								<option value="10" <?php if($field['font_size']=='10'){ echo 'selected'; } ?>> 10 </option>
								<option value="20" <?php if($field['font_size']=='20'){ echo 'selected'; } ?>> 20 </option>
								<option value="300" <?php if($field['font_size']=='30'){ echo 'selected'; } ?>> 30 </option>
							</select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_style]">
								<option value="italic" <?php if($field['font_style']=='italic'){ echo 'selected'; } ?>> Italic </option>
								<option value="normal" <?php if($field['font_style']=='normal'){ echo 'selected'; } ?>> Normal </option>
								<option value="initial" <?php if($field['font_style']=='initial'){ echo 'selected'; } ?>> Initial </option>
							</select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_color]">
								<option value="#fff" <?php if($field['font_color']=='#fff'){ echo 'selected'; } ?>> White </option>
								<option value="#000" <?php if($field['font_color']=='#000'){ echo 'selected'; } ?>> Black </option>
								<option value="#FF0000" <?php if($field['font_color']=='#FF0000'){ echo 'selected'; } ?>> Red </option>
							</select></td>
							<td><select name="data[f_<?=$field['id'];?>][text_align]">
								<option value="right" <?php if($field['text_align']=='right'){ echo 'selected'; } ?>> Right </option>
								<option value="center" <?php if($field['text_align']=='center'){ echo 'selected'; } ?>> Center </option>
								<option value="left" <?php if($field['text_align']=='left'){ echo 'selected'; } ?>> Left </option>
							</select></td>
							<td><input name="data[f_<?=$field['id'];?>][content]" type="text" value="<?=$field['content'];?>" /></td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>
     <div style="display:none"> 
      <canvas id="the-canvas">
      </canvas>
  </div>
  <div id="urlDownload"> 
</div>
 </div>
 <div id="lodaing" style="text-align: center; display:none;">
 	<img src="<?=\Uri::create('assets')?>/img/Preloader_21.gif"
 </div>
</form>
<script>
	var fid = 1;
	$(document).ready(function() {
		$('#btn_preview').click(function() {
			$.post(baseUrl + 'admin/templates/preview.json', $('#data_form').serialize(), function(json) {
				window.open(json.url, "pdfpreview");
			});
		});
		$('#btn_refresh').click(function() {	
			$.post(baseUrl + 'admin/templates/preview.json', $('#data_form').serialize(), function(json) {				
				$('#iframe').html('<iframe src="'+json.url+'" width="570" height="600"></iframe>') ;
		});
	  });
		$('#btn_save').click(function() {
			$.post(baseUrl + 'admin/templates/save.json', $('#data_form').serialize(), function(data) {				
              $('#lodaing').show();
              $('#main_div').hide();
             setTimeout(function(){
				createImage();
			   }, 5000);
			});
		});

	
		$('#btn_load').click(function() {
			$('#data_form').submit();
		});
		$('#btn_addpage').click(function() {
			$('#page').val('new');
			$('#data_form').submit();
		});
		$('#btn_addfield').click(function() {
			$('#fields_id').append('<option vlaue="'+fid+'">'+fid+'</option>');
			var html =  '<tr>'+
							'<td><input name="data[a'+fid+'][id_field]" type="text" size="5" /></td>'+
							'<td><select name="data[a'+fid+'][field_name]"><?php echo Get_fieldsValue('Field_Name'); ?></select></td>'+
							'<td><input name="data[a'+fid+'][location]" type="text" size="2" /></td>'+
							'<td><input name="data[a'+fid+'][pos_x]" type="text" size="5" /></td>'+
							'<td><input name="data[a'+fid+'][pos_y]" type="text" size="5" /></td>'+
							'<td><select name="data[a'+fid+'][width]"><?php echo Get_fieldsValue('width'); ?></select></td>'+
							'<td><select name="data[a'+fid+'][height]"><?php echo Get_fieldsValue('Height'); ?></select></td>'+
							'<td><select name="data[a'+fid+'][font_id]"><?php echo Get_fieldsValue('Font'); ?></select></td>'+
							'<td><select name="data[a'+fid+'][font_size]"><?php echo Get_fieldsValue('Font_Size'); ?></select></td>'+
							'<td><select name="data[a'+fid+'][font_style]"><?php echo Get_fieldsValue('Font_Style'); ?></select></td>'+
							'<td><select name="data[a'+fid+'][font_color]"><?php echo Get_fieldsValue('Font_Color'); ?></select></td>'+
							'<td><select name="data[a'+fid+'][text_align]"><?php echo Get_fieldsValue('Text_Align'); ?></select></td>'+
							'<td><input name="data[a'+fid+'][content]" type="text" /></td>'+
						'</tr>';
			fid++;
			$('#datalist > tbody:last').append(html);

		});
        $('#goField').click(function(){
             var v=$('#fields_id').val();            
             $('input[name="data[a'+v+'][id_field]"]').focus();
        });
		$('#fileupload').uploadFile({
			url: baseUrl + 'admin/templates/doupload.json',
			dragDrop: false,
			fileName: 'myfile',
			allowedTypes: 'jpg,jpeg',
			dynamicFormData: function() {
				var data = {template: $('#id_template').val(), page: $('#page').val()}
				return data;
			},
			returnType: 'json',
			onSuccess: function(json) {
				jQuery('#photo').val(json[0]);
			}
		});
		$('#btn_logout').click(function() {
			window.location = baseUrl + 'admin/login/logout';
		});
	});
	function createImage(){
		var id=$('#id_template').val();
		var page=parseInt($('#page').val())+1;
		var pdf_name=id+'_'+page;
		  PDFJS.getDocument( baseUrl +'assets/pdf/'+pdf_name+'.pdf').then(function(pdf) {
			  // you can now use *pdf* here
			    pdf.getPage(1).then(function(page) {
			    var scale = 1.5;
			    var viewport = page.getViewport(scale);
 
			    //
			    // Prepare canvas using PDF page dimensions
			    //
			    var canvas = document.getElementById('the-canvas');
			    var context = canvas.getContext('2d');
			    canvas.height = viewport.height;
			    canvas.width = viewport.width;

			    //
			    // Render PDF page into canvas context
			    //
			    var renderContext = {
			      canvasContext: context,
			      viewport: viewport
			    };
			    page.render(renderContext);
			    setTimeout(function(){
			     var dataURL = canvas.toDataURL();	
			     var ajax = new XMLHttpRequest();
				 ajax.open("POST",''+baseUrl+ 'admin/templates/saveImage.json',false);
				 ajax.setRequestHeader('Content-Type', 'application/upload');
				 var datas=dataURL+'-'+pdf_name;
				 $('#lodaing').hide();
				 $('#main_div').show();
				 ajax.send(datas );
				 alert('Data Saved');
				 $('#urlDownload').html('<h6>Download pdf</h6> <a href= "'+baseUrl +'assets/pdf/'+pdf_name+'.pdf" download>'+pdf_name+'</a> <br/> <h6>Download Image</h6> <a href="'+baseUrl +'assets/imagePdf/'+pdf_name+'.png" download>'+pdf_name+'</a>')
               }, 9000);
			   		    
			
			  });
			});
		  }
</script>
<script type="text/javascript" src="<?=\Uri::create('assets')?>/js/html2canvas.js"></script>
<script type="text/javascript" src="<?=\Uri::create('assets')?>/js/pdf.js"></script>

<style>

</style>
