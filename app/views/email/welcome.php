<?=\View::forge('email/email_header');?>
<!-- START CENTERED WHITE CONTAINER -->
<span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
<table class="main">
	<!-- START MAIN CONTENT AREA -->
	<tr>
		<td class="wrapper">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<p>Hi there,</p>
						<p>Sometimes you just want to send a simple HTML email with a simple design and clear call to action. This is it.</p>
						<p class="red">Please click the button to activate your account</p>
						<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
							<tbody>
								<tr>
									<td align="left">
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td><a href="<?=\Uri::create('activate/' . $user['token']);?>" target="_blank">Activate my account now</a></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<p>This is a really simple email template. It's sole purpose is to get the recipient to click the button with no distractions.</p>
						<p>Good luck! Hope it works.</p>
					</td>
				</tr>
			</table>
        </td>
    </tr>
    <!-- END MAIN CONTENT AREA -->
</table>
<?=\View::forge('email/email_footer');?>