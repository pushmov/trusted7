<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
		<?=\Asset::css($styles, array(), null, false);?>
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
		<script>var baseUrl = '<?=Uri::base(false);?>';</script>
		<?php if (isset($js_vars)) echo '<script>' . $js_vars . '</script>' . PHP_EOL;?>
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/modernizr.js"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
		<script src="https://use.fontawesome.com/da6c768a0e.js"></script>
		<script src="/assets/js/jquery.mask.js" type="text/javascript"></script>
		<script src="/assets/js/app.js" type="text/javascript"></script>
		<script>var USER_UNIT = "<?=\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit)?>";</script>
		<?=\Asset::js($scripts, array(), null, false);?>
	</head>

	<body class="member">
		<!--
		<div class="off-canvas-wrap move-right" data-offcanvas>
			<div class="inner-wrap">
				<a class="left-off-canvas-toggle" href="#"><i class="fi-list"></i></a>

				<aside class="left-off-canvas-menu">
					<div class="icon-bar vertical five-up">
						<div class="logo">
							<a href="/">TRUSTED</a>
						</div>
						<ul class="sidebar-nav">
							<li><a href="#">My Team</a></li>
							<li><a href="#">My Clients</a></li>
							<li><a href="#">My Templates</a></li>
							<li><a href="#">My Profile</a></li>
							<li><a href="#">Logout</a></li>
						</ul>
					</div>
				</aside>


			</div>
		</div>
		-->
		<div class="content">
			<nav>
				<h3 class="logo">Trusted And Referred</h3>
				<ul class="menu-right-nav">
					<li><a href="/users/profile"><i class="fa fa-user fa-lg"></i>My Profile</a></li>
					<li><a href="/logout"><i class="fa fa-sign-out fa-lg"></i>Logout</a></li>
				</ul>
			</nav>
			<div class="inner-content"><?=$content?></div>
		</div>
		<div id="dialog" class="reveal" data-reveal data-close-on-click="false"></div>
		<div id="dialog_medium" class="reveal medium" data-reveal data-close-on-click="false"></div>
		<div id="dialog_large" class="reveal large" data-reveal data-close-on-click="false"></div>
	</body>
</html>
