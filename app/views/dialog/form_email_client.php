<div class="modal-header">Email Client
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/client/send_message.json" id="form_send_client">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="first_name" class="right inline">Name <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="text" name="data[name]" id="name" value="<?=$data['full_names']?>">
					<input type="hidden" name="data[email]" id="email" value="<?=$data['email']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="first_name" class="right inline">Subject <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="text" name="data[subject]" id="subject" value="">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="first_name" class="right inline">Message <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<textarea name="data[message]" id="message"></textarea>
				</div>
			</div>
			
			<div class="row">
				<div class="small-12 medium-9 medium-offset-3 columns">
					<button type="button" class="button btn-primary" id="btn_send_email_client">Send Email</button>
					<button type="button" class="button btn-primary btn-close">Close</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>