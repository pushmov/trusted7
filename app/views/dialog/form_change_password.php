<div class="modal-header">Change Password
	<?php if (!isset($force) || ($force == 'false')): ?>
		<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
	<?php endif; ?>
</div>
<div class="modal-body text-center">
	<form class="form-basic" method="post" action="/users/profile/change_password.json" id="form_reset_password">
		<div class="row">
			<fieldset>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="passwrd" class="right inline">New password <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input type="password" name="data[passwrd]" id="passwrd" value="">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="cpasswrd" class="right inline">Confirm new password <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input type="password" name="data[cpasswrd]" id="cpasswrd" value="">
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<button type="button" class="button btn-primary" id="btn_update">Change Password</button>
					</div>
				</div>
			</fieldset>
		</div>
	</form>
</div>
