<div class="modal-header">Create new template
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/template/add.json" id="form_add_template">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="title" class="right inline">Title <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="text" name="data[title]" id="title" value="<?=$data['title']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="description" class="right inline">Description <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<textarea name="data[description]" id="description"><?=$data['description']?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="size" class="right inline">Size</label>
				</div>
				<div class="small-12 medium-9 columns">
					<?=\Form::select('size', \Model\Pagesize::DEFAULT_SIZE, \Model\Pagesize::forge()->get_size(), array('id' => 'size'))?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="width" class="right inline">Width <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-7 columns">
					<input type="text" name="data[width]" class="sizes" id="width" value="<?=$data['width']?>" readonly="true">
				</div>
				<div class="small-12 medium-2 columns">
					<span class="postfix"><?=\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit);?></span>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="height" class="right inline">Height <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-7 columns">
					<input type="text" name="data[height]" class="sizes" id="height" value="<?=$data['height']?>" readonly="true">
				</div>
				<div class="small-12 medium-2 columns">
					<span class="postfix"><?=\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit);?></span>
				</div>
			</div>
			<div class="row">
				<div class="small-offset-3 medium-3 columns">
					<button type="button" class="button btn-primary" id="btn_create_template">Create</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>