<div class="modal-header">Add Field
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/template/add_field.json" id="form_add_field">
		<?=\View::forge('dialog/template/form_field', array('data' => $data));?>
	</form>
</div>