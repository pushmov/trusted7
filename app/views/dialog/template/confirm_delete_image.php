<div class="modal-header">Confirm Delete
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/template/delete.json" id="form_remove_field">
		<div class="row text-center">
			<p>Are you sure want to delete this page image?</p>
			<div class="small-12 medium-12 columns">		
				<button type="button" class="btn btn-submit btn-primary danger" id="delete_page_image_submit">Delete</button>
			</div>
		</div>
	</form>
</div>