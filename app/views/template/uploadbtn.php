<div id="file_upload" class="template-upload">
</div>
<span id="txt_upload" data-val="<?=$txt?>"></span>
<script>
if($('#file_upload').length > 0) {
	$('#file_upload').uploadFile({
		url: '/users/template/doupload.json',
		dragDrop: false,
		fileName: 'myfile',
		allowedTypes: 'jpg,JPG,jpeg,JPEG,png,PNG',
		returnType: 'json',
        uploadStr:$('#txt_upload').attr('data-val'),
        dynamicFormData: function(){
            var data = {'template_id' : template_id, 'page_id': $('#page_dropdown').val()};
            return data;
        },
		showProgress: true,
        onSelect: function(){
            $('.page-image-control').find('.page-image-error').remove();
			$('.ajax-file-upload').text('Uploading...');
        },
		onSuccess: function(files,data,xhr) {
            if(data.error) {
                var m = '<div data-alert class="alert-box page-image-error danger">';
                m += '<ul class="unstyled">';
                m += '<li>' + data.error.message + '</li>';
                m += '</ul>';
                m += '</div>';
            } else {
                setPageImage();
                if (ft !== null) {
                    ft.unplug();
                    rect.remove();
                }
            }
            setObject(null);
            $('#save_placement').after(m);
			$.get('/users/template/upload_page.html', {page: $('#page_dropdown').val(), template: template_id}, function(html){
				$('#upload_page_image_btn').html(html);
			});
		}
	});
}
</script>