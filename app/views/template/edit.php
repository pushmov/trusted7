<form id="form_edit_fields" class="form-basic form-regular form-edit-fields" method="post" action="/users/template/save_field.json">
	
	<h5>Edit field</h5>
	<div class="item clearfix">
		<div class="small-12 medium-6 columns">
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="location" class="right inline">Location</label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="number" name="data[location]" id="location" value="<?=$data['location']?>" readonly="readonly">
					<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="pos_x" class="right inline">X-Coord</label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="number" name="data[pos_x]" id="pos_x" value="<?=$data['pos_x']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="pos_y" class="right inline">Y-Coord</label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="number" name="data[pos_y]" id="pos_y" value="<?=$data['pos_y']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="width" class="right inline">Width</label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="number" name="data[width]" id="width" value="<?=$data['width']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="height" class="right inline">Height</label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="number" name="data[height]" id="height" value="<?=$data['height']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="text_align" class="right inline">Align</label>
				</div>
				<div class="small-12 medium-9 columns">
					<?=\Form::select('data[text_align]', $data['text_align'], \Model\Field::forge()->text_align(), array('id' => 'text_align'))?>
				</div>
			</div>
		</div>
		<div class="small-12 medium-6 columns">
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="font_size" class="right inline">Font Size</label>
				</div>
				<div class="small-12 medium-9 columns">
					<?=\Form::select('data[font_size]', $data['font_size'], \Model\Field::forge()->font_size(), array('id' => 'font_size'))?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="font_style" class="right inline">Font Style</label>
				</div>
				<div class="small-12 medium-9 columns">
					<?=\Form::select('data[font_style]', $data['font_style'], \Model\Field::forge()->font_style(), array('id' => 'font_style'));?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="font_color" class="right inline">Font Color</label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="text" name="data[font_color]" id="font_color" class="color" value="<?=$data['font_color']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="type_id" class="right inline">Field Type</label>
				</div>
				<div class="small-12 medium-9 columns">
					<?=\Form::select('data[type_id]', $data['type_id'], \Model\Field::forge()->type_id(), array('id' => 'type_id'));?>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="content" class="right inline">Field Content</label>
				</div>
				<div class="small-12 medium-9 columns">
					<textarea id="content" name="data[content]"><?=$data['content']?></textarea>
				</div>
			</div>
		</div>
	</div>
</form>