<div class="row">
	<ul class="breadcrumbs">
		<li><a href="/users/dashboard">Dashboard</a></li>
		<li><a class="current" href="#">Template</a></li>
	</ul>

	<div class="small-12 column">
		<div class="clearfix">
			<form class="form-basic form-template" method="post" action="/users/template/update.json">
				<div class="template-header clearfix">
					<div class="small-12 medium-7 column">
						<div class="row">
							<div class="small-12 medium-4 column">
								<label for="title" class="right inline">Template Title <span class="astrix">*</span></label>
							</div>
							<div class="small-12 medium-8 column">
								<input type="text" name="data[title]" id="title" value="<?=$data['title']?>">
								<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 column">
								<label for="description" class="right inline">Template Description <span class="astrix">*</span></label>
							</div>
							<div class="small-12 medium-8 column">
								<textarea name="data[description]" id="description"><?=$data['description']?></textarea>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 column">
								<label for="team_slogan" class="right inline">Template Status</label>
							</div>
							<div class="small-12 medium-8 column">
								<div class="row">
									<div class="small-12 medium-5 column">
										<?=\Form::select('data[status_id]', $data['status_id'], \Model\Template::forge()->get_status_array());?>
									</div>
									<div class="small-12 medium-7 column text-right">
										<button type="button" class="button" id="btn_template_update">Save Template</button>
										<button type="button" class="button" id="btn_template_preview">Preview Template</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="small-12 medium-5 column">
						<div class="row">
							<div class="small-12 medium-3 column">
								<label for="size" class="right inline">Template Size</label>
							</div>
							<div class="small-12 medium-9 column">
								<?=\Form::select('data[size]', $data['size'], \Model\Pagesize::forge()->get_size(), array('disabled' => 'disabled', 'id' => 'size'))?>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-3 column">
								<label for="width" class="right inline">Template Width</label>
							</div>
							<div class="small-12 medium-7 column">
								<input type="text" name="data[width]" id="width" class="sizes" value="<?=$data['width']?>" disabled="disabled">
							</div>
							<div class="small-12 medium-2 column">
								<div class="postfix"><?=\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit); ?></div>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-3 column">
								<label for="width" class="right inline">Template Height</label>
							</div>
							<div class="small-12 medium-7 column">
								<input type="text" name="data[height]" id="height" class="sizes" value="<?=$data['height']?>" disabled="disabled">
							</div>
							<div class="small-12 medium-2 column">
								<div class="postfix"><?=\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit); ?></div>
							</div>
						</div>

					</div>
					<div id="notification"></div>
				</div>
			</form>

			<div class="template-body">
				<div class="row">
					<div class="small-12 medium-12">
						<div class="page-action">
							<button type="button" class="button small" id="add_page">Add Page</button>
							<div id="page_dropdown_w" class="page-dropdown-w">
								Page # <?=$data['page_dropdown']?>
							</div>
							<?php if($data['page'] > 1) : ?>
								<button type="button" class="button alert small" id="delete_page">Delete Page</button>
								<?php endif; ?>
							<!--<button type="button" class="button small" id="save_page">Save</button>-->
							<div id="upload_page_image_btn" class="upload-page-image-btn"></div>
							<input name="img_file" type="hidden" id="img_file" />
							
							<button type="button" class="button small save-placement" id="save_placement">Save Placement</button>
							<button type="button" class="button small" id="btn_page_preview">Preview Page</button>
							<button type="button" class="button small" id="add_field">Add Field</button>
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-12 large-5 column">
							<div class="page-image-control"></div>
							<div id="pageimage" class="pageimage">
								<div class="panel" id="panel"></div>
							</div>
						</div>
						<div class="small-12 medium-12 large-7 column">
							<div id="template_datatbl" class="template-tbl-fields">
								<table class="table table-striped row-border" id="tbl_fields">
									<thead>
										<tr>
											<th rowspan="2">Pos</th>
											<th rowspan="2">Placement <br> <small>x|y|width|height</small></th>
											<th rowspan="2">Shape</th>
											<th rowspan="2">Layer</th>
											<th class="center" colspan="4">Font</th>
											<th rowspan="2">Field Type</th>
											<th rowspan="2">Field Content</th>
											<th rowspan="2">Action</th>
										</tr>
										<tr>
											<th class="center">Align</th>
											<th class="center">Size</th>
											<th class="center">Style</th>
											<th class="center">Color</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div id="section_edit"></div>
		</div>
	</div>
</div>
<script src="/assets/js/template.raphael.js" type="text/javascript"></script>
<script src="/assets/js/template.js" type="text/javascript"></script>