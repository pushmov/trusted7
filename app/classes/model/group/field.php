<?php
namespace Model\Group;

/**
* Stores the group's field information that can be used in the templates
*/
class Field extends \Model_Crud {
	const S_NEW = 0; // New not active
	const S_ACTIVE = 1;
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Info archived

	const T_TEXT = 0;  // static text
	const T_IMAGE = 1;  // image
	
	const IMG_DIR = 'assets/img/';
	const IMG_PATH = 'group/';

	protected static $_table_name = 'groups_field';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected static $_defaults =	array('id' => null, 'group_id' => 0, 'title' => '',	'type_id' => self::T_TEXT, 'content' => '',
		'created_at' => '', 'created_by' => 0, 'updated_at' => '', 'updated_by' => 0, 'status_id' => 0);
	protected static $_mass_whitelist =	array('id', 'group_id', 'title', 'type_id', 'content',
		'created_at', 'created_by', 'updated_at', 'updated_by', 'status_id');
	protected static $_rules = array('content' => 'required');
	protected static $_labels = array('title' => 'Group field title', 'content' => 'Group field details');
	protected $_status = array(0 => 'New', 'Active', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');
	
	protected function post_delete($result){
		$field = \Model\Field::find_one_by_content($this->id);
		if ($field !== null) {
			$field->delete();
		}
		return $result;
	}
	
	public function get_table_name(){
		return self::$_table_name;
	}
	
	public function get_status($val=null){
		if($val === null) {
			return $this->_status;
		}
		return isset($this->_status[$val]) ? $this->_status[$val] : $this->_status;
	}
	
	public function get_fields($params){
		$data = array();
		$fields = self::find(array(
			'where' => array(
				'group_id' => $params['group_id']
			),
			'order_by' => array(
				'title' => 'asc'
			)
		));
		if($fields !== null) {
			foreach($fields as $row) {
				$data[$row->id] = $row->title;
			}
		}
		return \Form::select('data[content]', $params['value'], $data, array('id' => 'content'));
	}
	
	public function get_type($val=null){
		$data = array(
			self::T_TEXT => 'Static Text',
			self::T_IMAGE => 'Image'
		);
		if($val === null) {
			return $data;
		}
		return isset($data[$val]) ? $data[$val] : $data;
	}
	
	public function get_data(){
		return self::$_defaults;
	}

	public function get_lists($params){
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$status = "(CASE ";
		foreach($this->_status as $val => $name){
			$status .= "WHEN groups_field.status_id = '{$val}' THEN '{$name}' ";
		}
		$status .= " END)";
		$type = "(CASE ";
		foreach($this->get_type() as $val => $name){
			$type .= "WHEN groups_field.type_id = '{$val}' THEN '{$name}' ";
		}
		$type .= " END)";
		$aColumns = array('title', 'type', 'content', 'status', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM "
			. "(SELECT id,group_id,title,{$type} AS type,{$status} AS status,content,updated_at"
			. " FROM " . self::$_table_name . ") AS tmp WHERE group_id = '{$params['group_id']}'";
		$data['recordsTotal'] = \DB::query($sql, \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " AND (";
			$sql .= "title LIKE '%{$params['search']['value']}%'";
			$sql .= " OR type LIKE '%{$params['search']['value']}%'";
			$sql .= " OR content LIKE '%{$params['search']['value']}%'";
			$sql .= " OR status LIKE '%{$params['search']['value']}%'";
			$sql .= ")";
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= " ORDER BY {$aColumns[$params['order'][0]['column']]} {$params['order'][0]['dir']}";
		} else {
			$sql .= " ORDER BY updated_at DESC";
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT {$params['start']}, {$params['length']}";
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		$aColumns[] = 'action';
		foreach ($result as $row) {
			$row['action']  = \Form::button('button', '<i class="fa fa-edit"></i>Edit', array('class' => 'button small btn-group-field-edit', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-trash"></i>Remove', array('class' => 'button small alert btn-group-field-delete', 'type' => 'button', 'data-id' => $row['id']));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function delete_field($data){
		$response = array();
		$field = self::find_by_pk($data['id']);
		if ($field === null) {
			return true;
		}
		$file = \Files::forge()->load(\Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH . str_pad($data['id'], 8, '0', STR_PAD_LEFT));
		if($file) {
			unlink(DOCROOT.$file);
		}
		$field->delete();
		$response['success'] = true;
		return $response;
	}
	
	public function update_field($data){
		$response = array();
		
		if($data['id'] == '') {
			$data['id'] = null;
			$data['created_by'] = $data['updated_by'] = $data['user_id'];
			$model = self::forge($data);
		} else {
			$model = self::find_by_pk($data['id']);
			$data['updated_by'] = $data['user_id'];
			$model->set($data);
		}
		$val = $model->validation();
		$val->add_callable('rules');
		$val->add_callable('\Model\Field');
		$val->add_field('pos_x', 'X-Coord', 'numeric_min['.$this->get_min_pos_x().']|numeric_max['.$this->get_max_pos_x().']');
		$val->add_field('pos_y', 'Y-Coord', 'numeric_min['.$this->get_min_pos_y().']|numeric_max['.$this->get_max_pos_y().']');
		if($data['id'] != '') {
			$val->add_field('title', 'Title', 'required|unique_title_self['.$model->id.']');
		} else {
			$val->add_field('title', 'Title', 'required|unique_title');
		}
		if($model->validates()) {
			$model->save();
			if(isset($data['data_id']) && $data['type_id'] == self::T_IMAGE) {
				$insert = $model->to_array();
				$file = \Files::forge()->load(\Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH . $data['data_id']);
				if($file) {
					$ext = pathinfo(DOCROOT.$file, PATHINFO_EXTENSION);
					$filename = str_pad($insert['id'], 8, '0', STR_PAD_LEFT).'.'.$ext;
					rename(DOCROOT.$file, \Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH .$filename);
				}
			}
			$response['success']['message'] = 'Group field updated';
		} else {
			$response['error']['message'] = $val->error_message();
		}
		return $response;
	}
	
	public function get_field_title($group_id){
		$data = array();
		$result = self::find(array(
			'select' => array('id', 'title'),
			'where' => array(
				'group_id' => $group_id
			),
			'order_by' => array('title' => 'asc')
		));
		if($result !== null) {
			foreach($result as $row) {
				$data[$row->id] = $row->title;
			}
		}
		return $data;
	}
	
	public function get_group_id($params){
		$data = null;
		if($params['type_id'] == \Model\Field::T_GFIELD) {
			$field = self::find_by_pk($params['content']);
			$data = $field->group_id;
		}
		return $data;
	}

}