<?php
namespace Model;

class Testdrive extends \Model_Crud {
	const PDF_PATH = 'assets/pdf_images/';
	protected $_table_name = 'test_drive';
	protected $_data =
	array('id' => NULL, 'uuid' => '', 'first_name' => '', 'last_name' => '', 'company' => '', 'phone_cell' => '',
		'email' => '', 'website' => '', 'client' => '', 'address' => '', 'city' => '', 'state' => '', 'zip' => '',
		'date_created' => '');

	protected $_valrules =	array(
		'first_name' => array('First name', 'required'),
		'last_name' => array('Last name', 'required'),
		'company' => array('Company name', 'required'),
		'phone_cell' => array('Phone number', 'required|valid_phone'),
		'email' => array('Email', 'required|valid_email'),
		//'website' => array('Web Site', 'valid_url'),
		'client' => array('Client name', 'required'),
		'address' => array('Address', 'required'),
		'city' => array('City', 'required'),
		//'state' => array('Provice/State', 'required'),
		//'zip' => array('Postal/Zip code', 'required'),
	);

	public function get_datatable_list($params) {
		require_once 'classes/helper.class.php';
		$sql =
		"SELECT id,id_field,location,pos_x,pos_y,width,height,font_id,font_size,font_style,font_color,text_align,content FROM {$this->_table} WHERE id_template = {$params['id_template']} AND page = {$params['page']} ORDER BY location";
		$stmt = $this->db->rawquery($sql);
		$data = $stmt->fetchAll();
		return $data;
	}

	public function load_by_uuid($uuid) {
		$this->load(\DB::select_array()->where('uuid', '=', $uuid));
		return $this;
	}





	public function send($post) {
		$this->set_fields(array_map('trim', $post['data']));

		$validation = \Validation::forge();
		// Common rules
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, $rules[0], $rules[1]);
		}
		if (!$validation->run($this->as_array())) {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
		$this->uuid = md5(uniqid("tar!", TRUE));
		$this->date_created = date_create()->format('Y-m-d H:i:s');
		$this->save();
		if ($this->id) {
			if ($post['img_file'] != '') {
				// Rename the file to match the id
				foreach (array('png', 'jpg', 'jpeg') as $ext) {
					$fileName = DOCROOT . self::PDF_PATH . $post['img_file'];
					if (is_readable($fileName)) {
						rename($fileName, DOCROOT . self::PDF_PATH . 'test_' . str_pad($this->id, 3, '0', STR_PAD_LEFT) . '.' . $ext);
						break;
					}
				}
			}
			\Package::load('email');
			$email = \Email::forge();
			$url = \Uri::create('testdrive/preview/' . $this->uuid);

			$email->subject('Test Drive Sample');
			$email->to($this->email);
			$email->html_body(\View::forge('sample_email', array('url' => $url)));

			try	{
				$email->send();
			} catch(\EmailValidationFailedException $e)	{
				// The validation failed
			} catch(\EmailSendingFailedException $e) {
				// The driver could not send the email
			}

			// Send copy to admin??
		}
	}

	public function gen_pdf($temp = 1, $page = 0, $as_email = FALSE,$status = NULL) {
		$placeholders = array();
		$fields = array();

		$template = \DB::select()->from('pdf_templates')->where('id', $temp)->execute()->current();
		$result = \DB::query('SELECT * FROM pdf_fields')->execute();


		foreach($result as $row) {
			$fields[$row['id']] = $row;
		}
		$this->_data['realtor'] = $this->first_name . ' ' . $this->last_name;

		// Placeholders
		foreach ($this->_data as $k => $v) {
			$placeholders['{{' . $k . '}}'] = $v;
		}

		\Package::load('tcpdf');

		//		require_once 'tcpdf/config/tcpdf_config.php';
		//		require_once 'tcpdf/tcpdf.php';

		$pdf = new \TCPDF('P', 'mm', 'Letter');

		// Set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor("Trusted and Referred");
		$pdf->SetTitle("Trusted and Referred");
		$pdf->SetSubject("Trusted and Referred");

		// Remove default header/footer
		$pdf->setPrintHeader(FALSE);
		$pdf->setPrintFooter(FALSE);
		$pdf->SetMargins(0, 0, -1);
		$pdf->SetAutoPageBreak(FALSE, 0);

		if ($page != 0) {
			$sp = $page;
			$ep = $page;
		} else {
			$sp = 1;
			$ep = $template['pages'];
		}



		// Determine the house image
		foreach (array('png', 'jpg', 'jpeg') as $ext) {
			$fileName = DOCROOT . self::PDF_PATH . 'test_' . str_pad($this->id, 3, '0', STR_PAD_LEFT) . '.'	. $ext;

			if (is_readable($fileName)) {
				$photo_h = $fileName;
				break;
			}
		}


		if (!isset($photo_h)) {
			$photo_h = DOCROOT . self::PDF_PATH . str_pad($temp, 3, '0', STR_PAD_LEFT) . '_photo_h.jpg';
		}

		for ($page = $sp; $page <= $ep; $page++) {
			$pdf->AddPage();
			// Set bacKground image

				//echo '<hr /><br />page is '.$page.' --> ';

			$img_file = DOCROOT . self::PDF_PATH . str_pad($template['id'], 3, '0', STR_PAD_LEFT) . '_'
			. str_pad($page, 3, '0', STR_PAD_LEFT) . '.jpg';
			if (is_readable($img_file)) {
				$pdf->Image($img_file, 0, 0, 215.9, 279.4, '', '', '', FALSE, 150, '', FALSE, FALSE, 0);
				$result = \DB::select()->from('pdf_field_pos')->where('id_template', $template['id'])->where('page', $page)->execute();

				$dd = '';
				foreach($result as $row) {
					$dd[] = $row;
				}
			//	echo '<pre>'; print_r($result); print_r($dd);

				foreach($result as $row) {
					if ($fields[$row['id_field']]['id_status'] == 3) {
						// only image now is the house
						if ($row['id_field'] == 11) {
							if (is_readable($photo_h)) {
								if ($row['width'] > 0) {
									$width = $row['width'];
									$height = 0;
								} else {
									$width = 0;
									$height = $row['height'];
								}
								$pdf->Image($photo_h, $row['pos_x'], $row['pos_y'], $width, $height, '', '', '', FALSE, 150,
									'', FALSE, FALSE, 0);
							}
						}
					} else {
						$pdf->SetTextColorArray(explode(',', ($row['font_color'] == '') ? '0,0,0' : $row['font_color']));
						$pdf->SetFont('times', $row['font_style'], $row['font_size']);
						$pdf->SetXY($row['pos_x'], $row['pos_y']);

						if ($row['content'] == '') {
							if (isset($this->_data[$fields[$row['id_field']]['field']])) {
								$pdf->Cell($row['width'], 0, $this->_data[$fields[$row['id_field']]['field']], 0, 0,
									$row['text_align']);
							} else {
								$pdf->Cell($row['width'], 0, '', 0, 0, $row['text_align']);
							}
						} else {
							$string=$row['id_field'].' '.$row['content'];
							$str = strtr($string, $placeholders);
							$pdf->Cell($row['width'], 0, $str, 0, 0, $row['text_align']);
						}
					}
				}
			}
		}

		//echo '<pre>'; print_r($placeholders);
		//echo '<pre>'; print_r($fields);
		//die('good');
		// Reset pointer to the last page
		$pdf->lastPage();
		// Close and output PDF document
		if ($as_email) {
			return $pdf->Output('testdrive.pdf', 'S');
		} else {
			if($status=='OK'){
			 $pdf->Output('assets/pdf/'.$temp.'_'.$page.'.pdf', 'F');
			}else{
			 $pdf->Output('testdrive.pdf', 'I');
		    }
		}
	}
}