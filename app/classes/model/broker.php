<?php
namespace Model;

class Broker extends \Model_Crud {

	protected static $_table_name = 'brokers';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected static $_defaults =	array('id' => NULL, 'board_id' => '', 'name' => '', 'address' => '', 'city' => '', 'state' => '',
		'zip' => '', 'country' => '', 'phone' => '', 'website' => '', 'created_at' => '', 'updated_at' => '', 'status_id' => 0);
	protected static $_mass_whitelist = array('id', 'board_id', 'name', 'address', 'city', 'state', 'zip', 'country', 'phone', 'website',
		'created_at', 'updated_at', 'status_id');
	protected static $_rules = array('board_id' => 'required|unique_board', 'name' => 'required', 'address' => 'required', 'city' => 'required',
		'state' => 'required', 'zip' => 'required', 'country' => 'required', 'phone' => 'required', 'website' => 'valid_domain');
	protected static $_labels = array('name' => 'Name', 'address' => 'Address', 'city' => 'City', 'state' => 'State', 'zip' => 'ZIP Code',
		'country' => 'Country', 'phone' => 'Phone', 'website' => 'Website');
	protected $_status = array(0 => 'New', 'Active', self::S_HOLD => 'Hold', self::S_HOLD => 'Closed');
	// These fields can be used in the pdf
	protected static $_pdf_fields =	array('name' => 'Broker Name', 'address' => 'Broker Address', 'city' => 'Broker City', 'state' => 'Broker State',
		'zip' => 'Broker Zip', 'country' => 'Broker Country', 'phone' => 'Broker Phone', 'website' => 'Broker Website');

	public static function _validation_unique_board($value) {
		$val = \Validation::active();
		$result = self::find(array('where' => array('board_id' => trim($value), 'country' => $val->input('country'))));
		return ($result === null) ? true : false;
	}
	
	public function get_fields() {
		return self::$_pdf_fields;
	}

	public function get_status_array() {
		return $this->_status;
	}

	protected function prep_values($data) {
		$data['phone'] = preg_replace('/\D+/', '', $data['phone']);
		return $data;
	}

	public function get_all() {
		$data = array('' => 'Please select broker', 0 => 'New broker');
		$brokers = self::find(array(
			'select' => array('id', 'board_id', 'name', 'city', 'country'),
			'where' => array(
				'status_id' => self::S_ACTIVE
			)
		));
		if ($brokers) {
			foreach($brokers as $row) {
				$data[$row['id']] = $row['board_id'].' - '.$row['name'].', '.$row['city'].', '.\StateInfo::$country[$row['country']];
			}
		}
		return $data;
	}

	public function get_one($id){
		$data = self::find_by_pk($id);
		if(!$data) {
			$data = $this->_data;
		}
		return $data;
	}
}
