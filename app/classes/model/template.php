<?php
namespace Model;

class Template extends \Model_Crud {
	const S_NEW = 0; // New
	const S_ACTIVE = 1;
	const S_PAUSED = 2; // Not in use right now

	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Info archived
	const IMG_PLACEHOLDER = 'assets/img/template/noimage.jpg';

	protected static $_table_name = 'templates';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	// created_by = 0 will be a system template.
	protected static $_defaults = array('id' => NULL, 'title' => '', 'description' => '', 'size' => \Model\Pagesize::DEFAULT_SIZE, 'width' => 215.9,
		'height' => 279.4, 'created_at' => '', 'created_by' => 0, 'updated_at' => '', 'updated_by' => 0, 'status_id' => 0);
	protected static $_mass_whitelist = array('id', 'team_id', 'title', 'description', 'width', 'height', 'size', 'created_at', 'created_by', 'updated_at',
		'updated_by', 'status_id');
	protected static $_rules = array('title' => 'required', 'description' => 'required', 'width' => 'numeric_min[10]|numeric_max[215.9]', 'height' => 'numeric_min[10]|numeric_max[279.4]');
	protected static $_labels = array('title' => 'Template Title', 'description' => 'Template Description');
	protected $_status = array(0 => 'New', 'Active', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Closed');
	
	public function get_status_array() {
		return $this->_status;
	}
	
	protected static function post_find($result) {
		if ($result !== null && \Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			foreach ($result as $idx => $row) {
				$row->width = round($row->width * \Model\Pagesize::MM_IN, 1);
				$row->height = round($row->height * \Model\Pagesize::MM_IN, 1);
				$result[$idx] = $row;
			}
		}
		return $result;
	}
	
	protected function prep_values($data) {
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			$data['width'] = round($data['width'] / \Model\Pagesize::MM_IN, 1);
			$data['height'] = round($data['height'] / \Model\Pagesize::MM_IN, 1);
		}
		return $data;
	}

	public function get_data(){
		$data = self::$_defaults;
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			$data['width'] = round($data['width'] * \Model\Pagesize::MM_IN, 1);
			$data['height'] = round($data['height'] * \Model\Pagesize::MM_IN, 1);
		}
		return $data;
	}

	public function my_templates($params) {
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$sqlStatus = "(CASE ";
		foreach($this->_status as $val => $name){
			$sqlStatus .= "WHEN status_id = '{$val}' THEN '{$name}'";
		}
		$sqlStatus .= " END)";
		$aColumns = array('title', 'description', 'width', 'height', 'status');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM "
			. "(SELECT id,title,width,height,description,{$sqlStatus} AS status,created_by FROM ".self::$_table_name.") AS tmp"
			. " WHERE created_by = '{$params['user_id']}'";
		$data['recordsTotal'] = \DB::query($sql, \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " AND (";
			$sql .= "title LIKE '%{$params['search']['value']}%'";
			$sql .= " OR width LIKE '%{$params['search']['value']}%'";
			$sql .= " OR height LIKE '%{$params['search']['value']}%'";
			$sql .= " OR description LIKE '%{$params['search']['value']}%'";
			$sql .= " OR status LIKE '%{$params['search']['value']}%'";
			$sql .= ")";
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= " ORDER BY {$aColumns[$params['order'][0]['column']]} {$params['order'][0]['dir']}";
		} else {
			$sql .= " ORDER BY updated_at DESC";
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT {$params['start']}, {$params['length']}";
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();
		$aColumns[] = 'action';
		foreach ($result as $row) {
			if(\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
				$row['width'] = round($row['width'] * \Model\Pagesize::MM_IN, 1);
				$row['height'] = round($row['height'] * \Model\Pagesize::MM_IN, 1);
			}
			$row['width'] .= ' '.\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit);
			$row['height'] .= ' '.\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit);
			$row['action']  = \Form::button('button', '<i class="fa fa-eye"></i>View', array('class' => 'button small btn-template-view', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-edit"></i>Edit', array('class' => 'button small btn-template-edit', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-trash"></i>Delete', array('class' => 'button small alert btn-template-delete', 'type' => 'button', 'data-id' => $row['id']));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}
	
	public function update_template($data){
		$response = array();
		if(isset($data['id'])) {
			$template = self::find_by_pk($data['id']);
		} else {
			$template = self::forge($data);
		}
		if ($template === null) {
			unset($data['id']);
			$data['created_by'] = $data['updated_by'] = $data['user_id'];
			$template = self::forge($data);
		} else {
			$template->set($data);
		}
		if ($template->validates()) {
			$template->save();
			$response['success']['location'] = '/users/template/edit/'.$template->id;
			$response['success']['message'] = 'Data successfully updated';
		} else {
			$response['errors']['message'] = $template->validation()->error_message();
		}
		return $response;
	}
}
