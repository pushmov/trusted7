<?php
/**
*	Extra user information that may be required
*/
namespace Model\User;

class Extra extends \Model_Crud {
	protected static $_table_name = 'users_extra';

	protected static $_mass_whitelist =	array('id', 'phone', 'website');
	protected static $_defaults =	array('id' => NULL, 'phone' => '', 'website' => '');

  	protected function prep_values($data) {
		$data['phone'] = preg_replace('/\D+/', '', $data['phone']);
		return $data;
	}
}
