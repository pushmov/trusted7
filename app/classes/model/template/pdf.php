<?php
namespace Model\Template;

class Pdf extends \Model\Template {

	public function render($page = null, $output = 'I') {
		if (!$this->id === null) {
			return false;
		}
		$fields = new \Model\Field();
		\Package::load('tcpdf');
		$pdf = new \TCPDF('P', substr(\Model\User::forge()->get_unit(\Authlite::instance('auth_user')->get_user()->unit), 0, 2), array($this->width, $this->height));
		// Set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor("Trusted and Referred");
		$pdf->SetTitle($this->title);
		$pdf->SetSubject('');

		// Remove default header/footer and margins
		$pdf->setPrintHeader(FALSE);
		$pdf->setPrintFooter(FALSE);
		$pdf->SetMargins(0, 0, -1);
		$pdf->SetAutoPageBreak(FALSE, 0);

		// Do all pages if null

		if ($page === null) {
			$sp = 1;
			$ep = ($fields->total_page($this->id) <= 0) ? 1 : $fields->total_page($this->id);
		} else {
			$sp = $page;
			$ep = $page;
		}
		for ($page = $sp; $page <= $ep; $page++) {
			$pdf->AddPage();
			$bg = \Files::forge()->load_pdf_bg(\Model\Pageimage::IMG_DIR . \Model\Pageimage::IMG_PATH . str_pad($this->id, 8, '0', STR_PAD_LEFT) . '_' . $page);
			if(\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
				$m_w = round(($this->width / \Model\Pagesize::MM_IN) * \Model\Pageimage::PX_MM, 0);
				$m_h = round(($this->height / \Model\Pagesize::MM_IN) * \Model\Pageimage::PX_MM, 0);
			} else {
				$m_w = round($this->width * \Model\Pageimage::PX_MM, 0);
				$m_h = round($this->height * \Model\Pageimage::PX_MM, 0);
			}
			list($width, $height) = getimagesize(DOCROOT.$bg['file']);
			//checking if smaller than the canvas
			if($width < $m_w) {
				$ratio = $width / $m_w;
				$n_w = $ratio * $this->width;
			} else {
				$n_w = $this->width;
			}
			if($height < $m_h) {
				$ratio = $height / $m_h;
				$n_h = $ratio * $this->height;
			} else {
				$n_h = $this->height;
			}
			if ($bg['placeholder'] === true) {
				$bg_x = ($this->width / 2) - ($n_w / 2);
				$bg_y = ($this->height / 2) - ($n_h / 2);
			} else {
				$bg_x = $bg_y = 0;
			}
			$pdf->Image(DOCROOT.$bg['file'], $bg_x, $bg_y, $n_w, $n_h, '', '', '', true, 150, '', FALSE, FALSE, 0);
			$db = \DB::select(\Model\Field::forge()->get_table_name().'.*',
					array(\Model\Group\Field::forge()->get_table_name().'.content', 'group_field_content'),
					array(\Model\Group\Field::forge()->get_table_name().'.type_id', 'group_field_type_id'),
					array(\Model\Group\Field::forge()->get_table_name().'.id', 'group_field_id'));
			$db->from(\Model\Field::forge()->get_table_name());
			$db->join(\Model\Group\Field::forge()->get_table_name(), 'LEFT');
			$db->on(\Model\Field::forge()->get_table_name() . '.content', '=', \Model\Group\Field::forge()->get_table_name() . '.id');
			$db->where('template_id', '=', $this->id);
			$db->where('page', '=', $page);
			$result = $db->execute();
			foreach($result as $row) {
				if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
					$row['pos_x'] = round(($row['pos_x'] * \Model\Pagesize::MM_IN) , 2);
					$row['pos_y'] = round(($row['pos_y'] * \Model\Pagesize::MM_IN) , 2);
					$row['width'] = round(($row['width'] * \Model\Pagesize::MM_IN) , 2);
					$row['height'] = round(($row['height'] * \Model\Pagesize::MM_IN), 2);
				}
				$pdf->SetTextColorArray(array_map('trim', explode(',', ($row['font_color'] == '') ? '0,0,0' : trim(substr($row['font_color'], 3), '()'))));
				$pdf->SetFont('times', ($row['font_style'] == \Model\Field::FONT_NORMAL) ? '' : $row['font_style'], $row['font_size']);

				if(in_array($row['type_id'], \Model\Field::forge()->get_t_field_value())) {
					$fields = \Model\Field::field_content_model($row['type_id']);
					$ct = $fields[$row['content']];
				} else {
					$ct = $row['content'];
				}
				if($row['group_field_content'] != '') {
					$ct = $row['group_field_content'];
				}

				//custom field & image type content
				$gfile = \Files::forge()->load(\Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH . str_pad($row['group_field_id'], 8, '0', STR_PAD_LEFT));
				if ($row['type_id'] == \Model\Field::T_GFIELD
						&& $row['group_field_type_id'] == \Model\Group\Field::T_IMAGE
						&& $gfile) {
					$pdf->StartTransform();
					$ct = \Asset::img(str_replace(\Model\Group\Field::IMG_DIR, '', $gfile));
					if($row['shape_id'] == \Model\Field::SHAPE_RECT) {
						$pdf->Rect($row['pos_x'] , $row['pos_y'] , $row['width'] , $row['height'], 'CNZ', '');
					} elseif($row['shape_id'] == \Model\Field::SHAPE_CIRCLE) {
						$pdf->Circle($row['pos_x'] + ($row['height'] / 2), $row['pos_y'] + ($row['height'] / 2), $row['height'] / 2, 0, 360, 'CNZ');
					}
					$pdf->Image(DOCROOT.$gfile, $row['pos_x'], $row['pos_y'], $row['width'], '', '', true, '', false, 300);
					$pdf->StopTransform();
				} else {
					$pdf->writeHTMLCell($row['width'], 0, $row['pos_x'], $row['pos_y'], $ct, 0, 0, false, true, substr(\Model\Field::forge()->text_align($row['text_align']), 0, 1));
				}
			}
		}
		// Close and output PDF document
		if ($output == 'E') {
			return $pdf->Output('testdrive.pdf', 'E');
		} else {
			return $pdf->Output('trusted.pdf', $output);
		}
	}
}