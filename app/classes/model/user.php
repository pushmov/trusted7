<?php
namespace Model;

class User extends \Model_Crud {
	const S_NEW = 0; // New not validated
	const S_ACTIVE = 1;
	const S_PASSWORD = 2; // Needs new password
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Account closed, info archived

	const U_MILS = 0; // Millimeters
	const U_INCH = 1; // Inches

	const IMG_DIR = 'assets/img/';
	const IMG_PATH = 'member/';
	
	protected $_user_unit;

	protected static $_table_name = 'users';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected static $_mass_whitelist =	array('id', 'email', 'passwrd', 'first_name', 'last_name', 'unit',
		'token' => '', 'session', 'created_at',	'updated_at', 'parent_id', 'status_id');
	protected static $_defaults =	array('id' => NULL, 'email' => '', 'passwrd' => '','first_name' => '', 'last_name' => '',
	 	'unit' => self::U_INCH, 'token' => '', 'session' => '', 'created_at' => '', 'updated_at' => '', 'parent_id' => 0,
	 	'status_id' => 0);
	protected static $_rules = array('email' => 'required|valid_email', 'first_name' => 'required', 'last_name' => 'required');
	protected static $_labels = array('email' => 'Email', 'passwrd' => 'Password', 'first_name' => 'First Name', 'last_name' => 'Last Name');
	protected $_status = array(0 => 'New', 'Active', 'Password', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');
	protected $_units = array(self::U_MILS => 'mm', self::U_INCH => 'inch');
	protected static $_pdf_fields =	array('first_name' => 'User First name', 'last_name' => 'User Last Name', 'email' => 'User Email', 'website' => 'User Website', 'phone' => 'User Phone');
	
	public function get_user_unit(){
		return $this->_user_unit;
	}

	public function get_fields(){
		return self::$_pdf_fields;
	}
	
	public function get_unit($val = null){
		if($val === null) {
			return $this->_units;
		}
		return isset($this->_units[$val]) ? $this->_units[$val] : $this->_units;
	}

	public function get_status_array() {
		return $this->_status;
	}

	public function generate_token($id) {
		return md5(uniqid($id, true));
	}

	protected function pre_validate($data) {
		if ($data['email'] != $this->email) {
			static::$_rules['email'] .= '|unique_email[user]';
		}
		return $data;
	}

	public function profile($id) {
		$data = self::find_by_pk($id);
		return $data->to_array();
	}

	protected function post_save($result) {
		$this->passwrd = \Authlite::instance('auth_user')->hash($this->passwrd);
		$this->save(false);
		return $result;
	}

	public function all_users($params) {
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$sqlStatus = "(CASE ";
		foreach($this->_status as $val => $name){
			$sqlStatus .= " WHEN status_id = '{$val}' THEN '{$name}'";
		}
		$sqlStatus .= " END)";
		$aColumns = array('name', 'email', 'status');
		$sql = "SELECT SQL_CALC_FOUND_ROWS id,CONCAT(first_name,' ', last_name) as name,email,status_id,parent_id,{$sqlStatus} AS status FROM ".self::$_table_name
			. " WHERE (parent_id = '{$params['user_id']}' AND id <> {$params['user_id']} AND parent_id <> '0')";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " AND (";
			$sql .= "name LIKE '%{$params['search']['value']}%'";
			$sql .= " OR email LIKE '%{$params['search']['value']}%'";
			$sql .= " OR status LIKE '%{$params['search']['value']}%'";
			$sql .= ")";
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= " ORDER BY {$aColumns[$params['order'][0]['column']]} {$params['order'][0]['dir']}";
		} else {
			$sql .= " ORDER BY id";
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();
		$aColumns[] = 'action';
		foreach ($result as $row) {
			$row['action']  = \Form::button('button', '<i class="fa fa-eye"></i>Profile', array('class' => 'button small user-profile', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-trash"></i>Remove', array('class' => 'button small alert btn-user-remove', 'type' => 'button', 'data-id' => $row['id']));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function dologin($data){
		$response = array();
		$authlite = \Authlite::instance('auth_user');
		$authlite->logout();
		if ($authlite->login($data['email'], $data['passwrd'], isset($data['remember']))) {
			$status = $authlite->get_user()->status_id;
			switch ($status) {
				case self::S_NEW:
					$response['error']['message'] = 'Your account not yet activated. Please check your inbox for instruction';
					break;
				case self::S_HOLD:
					$response['error']['message'] = 'Your account was termprarily on hold.';
					break;
				case self::S_ARCHIVE:
					$response['error']['message'] = 'Your account archived';
					break;
				default: //success
					$response['success']['message'] = 'Login Successful';
					break;
			}
		} else {
			$response['error']['message'] = 'Invalid email or password';
		}
		return $response;
	}

	public function is_email_used($data){
		$user = self::find_one_by_email($data['email'], '=');
		if ($user === null) {
			$response['status'] = false;
			$response['success'] = array('message' => 'This email ready to use');
		} else {
			$response['status'] = true;
			$response['error'] = array('message' => 'This email already in use');
		}
		return $response;
	}

	public function reset_password($data){
		//todo : need create cron task to reset token, if user not requested reset_password or abandoned email reset instruction
		$user = self::find_one_by_email($data['email'], '=');
		if ($user !== null) {
			$user->token = $this->generate_token($data['email']);
			$user->status_id = self::S_PASSWORD;
			$user->save(false);
			\Emails::forge()->reset_password($user->to_array());
			$response['success'] = array('message' => 'We have emailed you for next instruction');
		} else {
			$response['error'] = array('message' => 'We could not find this email');
		}
		return $response;
	}

	public function remove($id){
		$response = array();
		$clients = \Model\Client::find_by_user_id($id);
		if ($clients !== null) {
			foreach ($clients as $client) {
				\Model\Client::forge()->remove($client->id);
			}
		}
		$user = self::find_by_pk($id);
		$p = $user->parent_id;
		if($user) {
			$user->delete();
		}
		$response['status'] = true;
		$users = self::find_by_parent_id($p);
		$response['count'] = count($users);
		return $response;
	}

	public function get_team_member($id){
		$user = self::find_by_pk($id);
		if($user === null) {
			return '';
		}
		$data = array();
		$data[] = $user->id;
		$users = self::find_by_parent_id($user->id);
		if(count($users) > 0) {
			foreach($users as $row) {
				$data[] = $row->id;
			}
		}
		return $data;
	}


}
