<?php
/**
*	Extra user information that may be required
*/
namespace Model;

class Pagesize extends \Model_Crud {
	const DEFAULT_SIZE = 2;
	const MIN_WH_VALUE = 10;
	const MAX_WH_VALUE = 10;

	const MM_IN = 0.03937; // mm to inches

	protected static $_table_name = 'page_sizes';

	protected static $_defaults =	array('id' => NULL, 'name' => '', 'width' => '', 'height' => '');
	protected static $_mass_whitelist =	array('id', 'name', 'width', 'height');

	public function get_size($val = null){
		if($val === null) {
			$sizes = self::find_all();
			foreach($sizes as $row) {
				$data[$row->id] = $row->name;
			}
		} else {
			$sizes = self::find_by_pk($val);
			if($sizes === null || $val == 0) {
				return $data;
			}
			$data[$sizes->id] = $sizes->name;
		}
		return $data;
	}

	public function get_width_height($params){
		$data = self::find_by_pk($params['id']);
		if($data['id'] != self::DEFAULT_SIZE) {
			$data['editable'] = true;
		}
		if(\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			$data['width'] = round($data['width'] * \Model\Pagesize::MM_IN, 1);
			$data['height'] = round($data['height'] * \Model\Pagesize::MM_IN, 1);
		}
		return $data;
	}

	public function is_custom($params){
		$response = array();
		$response['value'] = $params['size'];
		if($params['size'] == self::DEFAULT_SIZE) {
			$response['readonly'] = true;
		}
		return $response;
	}
}
