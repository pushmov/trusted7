<?php
namespace Model;

class Pageimage extends \Model_Crud {
	const MAX_DPI = 300;
	const PX_MM = 11.811; 	// pixels per mm @ 300dpi
	const DPI_CONST = 25.4;

	const IMG_DIR = 'assets/img/';
	const IMG_PATH = 'template/';

	protected $_exts = array('jpg','png');

	protected function upload_error($code) {
		switch ($code) {
			case UPLOAD_ERR_INI_SIZE:
				$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
				break;
			case UPLOAD_ERR_PARTIAL:
				$message = "The uploaded file was only partially uploaded";
				break;
			case UPLOAD_ERR_NO_FILE:
				$message = "No file was uploaded";
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$message = "Missing a temporary folder";
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$message = "Failed to write file to disk";
				break;
			case UPLOAD_ERR_EXTENSION:
				$message = "File upload stopped by extension";
				break;
			default:
				$message = "Unknown upload error";
				break;
		}
		return $message;
	}

	public function get_dpi($file) {
		$a = fopen($file,'r');
		$string = fread($a,20);
		fclose($a);
		$data = bin2hex(substr($string,14,4));
		$x = substr($data,0,4);
		$y = substr($data,4,4);
		return array('x' => hexdec($x), 'y' => hexdec($y));
	}


	public function get_image($data) {
		$response = array();
		$file = \Files::forge()->load(self::IMG_DIR . self::IMG_PATH . str_pad($data['template_id'], 8, '0', STR_PAD_LEFT) . '_'.$data['page_id']);
		$response['success'] = true;
		if($file === false) {
			list($width, $height, $type, $attr) = getimagesize(DOCROOT.self::IMG_DIR . self::IMG_PATH . 'noimage.jpg');
			$response['image'] = self::IMG_DIR . self::IMG_PATH . 'noimage.jpg'.'?v='.time();
			$response['attrs'] = array('width' => $width, 'height' => $height, 'type' => $type, 'attr' => $attr, 'placeholder' => true);
		} else {
			list($width, $height, $type, $attr) = getimagesize(DOCROOT.$file);
			$response['image'] = self::IMG_DIR . self::IMG_PATH . basename($file).'?v='.time();
			$response['attrs'] = array('width' => $width, 'height' => $height, 'type' => $type, 'attr' => $attr);
		}
		return $response;
	}

	public function delete_image($data) {
		$file = \Files::forge()->load(self::IMG_DIR . self::IMG_PATH . str_pad($data['template_id'], 8, '0', STR_PAD_LEFT) . '_'.$data['page_id']);
		if($file) {
			unlink(DOCROOT.$file);
		}
		return true;
	}

	public function doupload($data) {
		$template = \Model\Template::find_by_pk($data['template_id']);
		$output_dir = DOCROOT . self::IMG_DIR . self::IMG_PATH;
		$response = array();

		if (!is_dir($output_dir)){
			mkdir($output_dir);
		}
		if ($_FILES["myfile"]["error"]) {
			$response['error']['message'] = $this->upload_error($_FILES['myfile']['error']);
		} else {
			// Max width/hight
			$m_w = round($template['width'] * self::PX_MM, 0);
			$m_h = round($template['height'] * self::PX_MM, 0);
			$filename = str_pad($data['template_id'], 8, '0', STR_PAD_LEFT);
			$filename .= '_' . $data['page_id'];
			foreach($this->_exts as $ext) {
				$file = $output_dir . $filename . '.' . $ext;
				if(is_readable($file)){
					unlink($file);
				}
			}
			$ext = strtolower(pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION));
			$filename .= '.' . $ext;
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $filename);

			list($o_w, $o_h) = getimagesize($output_dir.$filename);
			$dpi = $this->get_dpi($output_dir . $filename); //return w,h in dpi
			$resize = false;
			if ($o_w > $m_w) {
				$n_w = $m_w;
				$resize = true;
			} else {
				$n_w = $o_w;
			}
			if ($o_h > $m_h) {
				$n_h = $m_h;
				$resize = true;
			} else {
				$n_h = $o_h;
			}

			if ($resize) {
				$ratio = $o_w / $o_h; // width/height

				if ($dpi['x'] < self::MAX_DPI) {
					$dpinum_x = $dpi['x'];
				} else {
					$dpinum_x = self::MAX_DPI;
				}

				if ($dpi['y'] >= self::MAX_DPI || $dpi['y'] == 0) {
					$dpinum_y = self::MAX_DPI;
				} else {
					$dpinum_y = $dpi['y'];
				}

				if ($ratio > 1) {
					$n_w = ($template['width'] * $dpinum_x) / self::DPI_CONST;
					$n_h = (($template['height'] * $dpinum_y) / self::DPI_CONST) / $ratio;
				} else {
					$n_w = ($template['width'] * $dpinum_x) / self::DPI_CONST;
					$n_h = (($template['height'] * $dpinum_y) / self::DPI_CONST) * $ratio;
				}
				$thumb = imagecreatetruecolor($n_w, $n_h);
				$jpgs = array('jpg', 'jpeg');
				if (in_array($ext, $jpgs)) {
					$source = imagecreatefromjpeg($output_dir.$filename);
					imagecopyresampled($thumb, $source, 0, 0, 0, 0, $n_w, $n_h, $o_w, $o_h);
					imagejpeg($thumb, $output_dir.$filename);
				} else {
					$source = imagecreatefrompng($output_dir.$filename);
					imagecopyresampled($thumb, $source, 0, 0, 0, 0, $n_w, $n_h, $o_w, $o_h);
					imagepng($thumb, $output_dir.$filename);
				}


			}
			$response['file'] = $filename;
			$response['dimension'] = $n_w.' x '.$n_h;
			$response['dpi'] = 'x: '.$dpi['x'].' & y: '.$dpi['y'];
		}
		$file = \Files::forge()->load(self::IMG_DIR . self::IMG_PATH . str_pad($data['template_id'], 8, '0', STR_PAD_LEFT) . '_'.$data['page_id']);
		$upload_status = ($file) ? 1 : 0;
		$response['image_status'] = $upload_status;
		return $response;
	}

	public function rename_pageimage($params){
		$file = \Files::forge()->load(self::IMG_DIR . self::IMG_PATH . str_pad($params['template_id'], 8, '0', STR_PAD_LEFT) . '_'.$params['oldpage']);
		if($file) {
			$name = $params['template_id'];
			$ext = pathinfo(DOCROOT.$file, PATHINFO_EXTENSION);
			$filename = str_pad($name, 8, '0', STR_PAD_LEFT);
			$filename .= '_' . $params['newpage'];
			$filename .= '.' . $ext;
			$outputdir = DOCROOT . self::IMG_DIR . self::IMG_PATH;
			rename(DOCROOT.$file, $outputdir.$filename);
		}
		return true;
	}

}