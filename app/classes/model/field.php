<?php
namespace Model;

/**
* Stores the field information for each of the templates
*/
class Field extends \Model_Crud {
	const S_NEW = 0; // New not active
	const S_ACTIVE = 1;
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Info archived

	const T_TEXT = 0;  // static text
	const T_UFIELD = 1;  // user database field
	const T_UIMG = 2;  // user image
	const T_GFIELD = 3; // custom group field
	const T_CFIELD = 7;  // client database field
	const T_CIMG = 8;  // client image

	const SHAPE_RECT = 0;
	const SHAPE_CIRCLE = 1;

	const FONT_SIZE_DEFAULT = 12;

	const TXT_LEFT = 0;
	const TXT_RIGHT = 1;
	const TXT_CENTER = 3;
	const TXT_JUSTIFY = 4;

	const FONT_NORMAL = 'N';
	const FONT_BOLD = 'B';
	const FONT_ITALIC = 'I';
	const FONT_UNDERLINE = 'U';

	//letter size x,y measurement in mm
	const MIN_X_COORD = 5.0;
	const MIN_Y_COORD = 5.0;
	const MAX_X_COORD = 210.0;
	const MAX_Y_COORD = 275.0;

	const MIN_WIDTH = 3.0;
	const MIN_HEIGHT = 3.0;

	const DEFAULT_RECT_W = 10;	//self::MIN_X_COORD + 5.0;
	const DEFAULT_RECT_H = 10;	//self::MIN_Y_COORD + 5.0;

	const MAX_CONTENT_CHAR = 30;
	
	protected static $_multiplier = 1;

	protected static $_table_name = 'fields';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected static $_defaults =	array('id' => null, 'template_id' => 0, 'page' => 0, 'location' => 1, 'layer' => 1, 'shape_id' => self::SHAPE_RECT,
	 	'pos_x' => self::MIN_X_COORD, 'pos_y' => self::MIN_Y_COORD, 'width' => self::DEFAULT_RECT_W, 'height' => self::DEFAULT_RECT_H,
	 	'rotate' => 0, 'font_id' => 0, 'font_size' => 12, 'font_style' => '', 'font_color' => 'rgb(0, 0, 0)', 'text_align' => 0,
	 	'type_id' => 0,	'content' => '', 'created_at' => '', 'created_by' => 0, 'updated_at' => '', 'updated_by' => 0, 'status_id' => 0);
	protected static $_mass_whitelist =	array('id', 'template_id', 'page', 'location', 'layer', 'shape_id', 'pos_x', 'pos_y', 'width',
		'height', 'rotate', 'font_id', 'font_size', 'font_style', 'font_color', 'text_align', 'type_id',
		'content', 'created_at', 'created_by', 'updated_at', 'updated_by', 'status_id');
	protected static $_rules = array(
		'width' => 'valid_max_width|valid_circle_width', 'height' => 'valid_max_height|valid_circle_height', 'font_color' => 'valid_rgb', 'layer' => 'numeric_min[1]|numeric_max[255]'
	);
	protected static $_labels = array('pos_x' => 'X-Coord', 'pos_y' => 'Y-Coord', 'width' => 'Width',
		'height' => 'Height', 'font_color' => 'Font color');

	protected $_status = array(0 => 'New', 'Active', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Closed');
	protected $_field_type = array(
		self::T_TEXT => 'Static text', self::T_UFIELD => 'User field', self::T_UIMG => 'User image',
		self::T_GFIELD => 'Custom field', self::T_CFIELD => 'Client field', self::T_CIMG => 'Client image'
	);
	protected $_font_style = array(
		self::FONT_NORMAL => 'Normal', self::FONT_BOLD => 'Bold', self::FONT_ITALIC => 'Italic', self::FONT_UNDERLINE => 'Underline'
	);
	protected $_text_align = array(
		self::TXT_LEFT => 'Left', self::TXT_RIGHT => 'Right', self::TXT_CENTER => 'Center', self::TXT_JUSTIFY => 'Justify'
	);
	protected $_t_field_value = array( self::T_UFIELD, self::T_CFIELD );
	protected $_t_image_value = array( self::T_UIMG, self::T_CIMG );
	
	protected static function post_find($result) {
		if ($result !== null) {
			foreach ($result as $idx => $row) {
				if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
					$row->width = round($row->width * \Model\Pagesize::MM_IN, 2);
					$row->height = round($row->height * \Model\Pagesize::MM_IN, 2);
					$row->pos_x = round($row->pos_x * \Model\Pagesize::MM_IN, 2);
					$row->pos_y = round($row->pos_y * \Model\Pagesize::MM_IN, 2);
				}
				$result[$idx] = $row;
			}
		}
		return $result;
	}
	
	public function get_min_pos_x() {
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return round(self::MIN_X_COORD * self::$_multiplier, 1);
	}
	
	public function get_max_pos_x(){
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return round(self::MAX_X_COORD * self::$_multiplier, 1);
	}
	
	public function get_min_pos_y() {
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return round(self::MIN_Y_COORD * self::$_multiplier, 1);
	}
	
	public function get_max_pos_y() {
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return round(self::MAX_Y_COORD * self::$_multiplier, 1);
	}

	public static function field_content_model($type) {
		$model = '';
		switch ($type) {
			case self::T_UFIELD :
				$model = '\\Model\\User';
				break;
			case self::T_CFIELD :
				$model = '\\Model\\Client';
				break;
			case self::T_GFIELD :
				$model = '\\Model\\Group\\Field';
				break;
		}
		return $model::forge()->get_fields();
	}

	protected function prep_values($data) {
		$data['pos_x'] = round($data['pos_x'], 2);
		$data['pos_y'] = round($data['pos_y'], 2);
		$data['width'] = round($data['width'], 2);
		$data['height'] = round($data['height'], 2);
		$data['rotate'] = round($data['rotate'], 2);
		if ($data['shape_id'] == self::SHAPE_CIRCLE) {
			if($data['width'] > $data['height']) {
				$data['height'] = $data['width'];
			} elseif($data['height'] > $data['width']) {
				$data['width'] = $data['height'];
			}
		}
		return $data;
	}

	protected static function _max_width($pos_x) {
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return round(abs((self::MIN_WIDTH) - (((self::MAX_X_COORD) - (self::MIN_X_COORD)) - (float)$pos_x)) * self::$_multiplier, 2);
	}

	protected static function _max_height($pos_y) {
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return round(abs((self::MIN_HEIGHT) - (((self::MAX_Y_COORD) - (self::MIN_Y_COORD)) - (float)$pos_y)) * self::$_multiplier, 2);
	}

	public function get_t_field_value(){
		return $this->_t_field_value;
	}

	public function get_table_name(){
		return self::$_table_name;
	}

	public function get_status_array() {
		return $this->_status;
	}

	public function type_id($val = null){
		if ($val === null) {
			return $this->_field_type;
		}
		return $this->_field_type[$val];
	}

	public function font_style($val = null){
		if ($val === null) {
			return $this->_font_style;
		}
		return $this->_font_style[$val];
	}

	public function text_align($val = null) {
		if ($val === null) {
			return $this->_text_align;
		}
		return $this->_text_align[$val];
	}

	public function font_size() {
		$data = array();
		for ( $i = 5; $i <= 50; $i ++ ) {
			$data[$i] = $i;
		}
		return $data;
	}

	public function shape($val = null){
		$data = array(
			self::SHAPE_RECT => 'Rectangle',
			self::SHAPE_CIRCLE => 'Circle'
		);
		if ($val === null) {
			return $data;
		}
		return $data[$val];
	}

	public static function _validation_valid_max_width($value) {
		$val = \Validation::active();
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return $value >= (self::MIN_WIDTH * self::$_multiplier) && $value <= self::_max_width((float)$val->input('pos_x'));
	}

	public static function _validation_valid_max_height($value) {
		$val = \Validation::active();
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			self::$_multiplier = \Model\Pagesize::MM_IN;
		}
		return $value >= (self::MIN_HEIGHT * self::$_multiplier) && $value <= self::_max_height((float)$val->input('pos_y'));
	}

	public static function _validation_valid_circle_width($value){
		$val = \Validation::active();
		if ($val->input('shape_id') == self::SHAPE_CIRCLE) {
			return ($value == $val->input('height'));
		} else {
			return true;
		}
	}

	public static function _validation_valid_circle_height($value){
		$val = \Validation::active();
		if ($val->input('shape_id') == self::SHAPE_CIRCLE) {
			return ($val->input('width') == $value);
		} else {
			return true;
		}
	}

	public function get_data(){
		return self::$_defaults;
	}

	public function total_page($template_id){
		$data = self::count('page', true, array(
			'template_id' => $template_id
		), 'page');
		return count($data);
	}

	protected function post_save($result){
		$results = \DB::select()->from(self::$_table_name)
		->where('template_id', '=', $this->template_id)
		->where('page', '=', $this->page)
		->execute()
		->as_array();
		if (count($results) > 1) {
			$rows = \DB::select()->from(self::$_table_name)->where('id', '<>', $this->id)
				->where('template_id', '=', $this->template_id)
				->where('page', '=', $this->page)
				->execute()
				->as_array();
			if ($rows) {
				$location = 2;
				foreach ($rows as $row) {
					\DB::update(self::$_table_name)
					->value('location', $location)
					->where('id', '=', $row['id'])
					->execute();
					$location++;
				}
			}
		}
		return $result;
	}


	public function get_fields($params) {
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$typeId = "(CASE ";
		foreach($this->_field_type as $val => $name){
			$typeId .= "WHEN ".self::$_table_name.".type_id = '{$val}' THEN '{$name}' ";
		}
		$typeId .= " END)";

		$alignText = "(CASE ";
		foreach($this->_text_align as $val => $name){
			$alignText .= "WHEN text_align = '{$val}' THEN '{$name}' ";
		}
		$alignText .= " END)";

		$fontStyle = "(CASE ";
		foreach($this->_font_style as $val => $name){
			$fontStyle .= "WHEN font_style = '{$val}' THEN '{$name}' ";
		}
		$fontStyle .= " END)";

		$shape = "(CASE ";
		foreach($this->shape() as $val => $name){
			$shape .= "WHEN shape_id = '{$val}' THEN '{$name}' ";
		}
		$shape .= " END)";

		$aColumns = array('location', 'placement','shape' ,'layer', 'align', 'font_size', 'style', 'font_color',
			'type', 'content', 'action');
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM "
			. "(SELECT ".self::$_table_name.".id,template_id,location,CONCAT(pos_x,'|',pos_y,'|',width,'|',height) AS placement,{$shape} AS shape,layer,{$alignText} AS align,font_size,{$fontStyle} AS style,page,font_color,".self::$_table_name.".type_id,".self::$_table_name.".content,{$typeId} AS type, ".\Model\Group\Field::forge()->get_table_name().".content AS group_field_content"
			. " FROM ".self::$_table_name
			. " LEFT JOIN ". \Model\Group\Field::forge()->get_table_name()
			. " ON ".self::$_table_name .".content = " .\Model\Group\Field::forge()->get_table_name().'.id'
			. ") AS tmp"
			. " WHERE template_id = '{$params['template_id']}' AND page = '{$params['page_dropdown']}'";
		$data['recordsTotal'] = \DB::query($sql, \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " AND (";
			$sql .= "location LIKE '%{$params['search']['value']}%'";
			$sql .= " OR placement LIKE '%{$params['search']['value']}%'";
			$sql .= " OR align LIKE '%{$params['search']['value']}%'";
			$sql .= " OR font_size LIKE '%{$params['search']['value']}%'";
			$sql .= " OR style LIKE '%{$params['search']['value']}%'";
			$sql .= " OR font_color LIKE '%{$params['search']['value']}%'";
			$sql .= " OR type LIKE '%{$params['search']['value']}%'";
			$sql .= " OR content LIKE '%{$params['search']['value']}%'";
			$sql .= ")";
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= " ORDER BY {$aColumns[$params['order'][0]['column']]} {$params['order'][0]['dir']}";
		} else {
			$sql .= " ORDER BY location ASC";
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT {$params['start']}, {$params['length']}";
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();
		function convert($val){
			return round($val / \Model\Pagesize::MM_IN, 2);
		}
		foreach ($result as $row) {
			if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
				$converted = array_map(function($val){
					return round($val * \Model\Pagesize::MM_IN, 2);
				}, explode('|', $row['placement']));
				$row['placement'] = join('|', $converted);
			}
			$row['placement'] = \Html::anchor('#', $row['placement'], array('class' => 'no-link placement-value', 'data-id' => $row['id']));
			$row['font_color'] = '<div class="rgb_font_color" style="background: '.$row['font_color'].'"></div>';
			if(in_array($row['type_id'], $this->_t_field_value)) {
				$fields = self::field_content_model($row['type_id']);
				$ct = $fields[$row['content']];
			} else {
				$ct = $row['content'];
			}
			if($row['group_field_content'] != '') {
				$ct = $row['group_field_content'];
			}
			$row['content'] = strlen($ct) > self::MAX_CONTENT_CHAR ? substr($ct, 0, self::MAX_CONTENT_CHAR)."..." : $ct;
			$row['action'] = \Form::button('button', '<i class="fa fa-edit fa-lg"></i>', array('class' => 'button small field-edit', 'type' => 'button', 'data-id' => $row['id'], 'title' => 'Edit'));
			$row['action']  .= \Form::button('button', '<i class="fa fa-trash fa-lg"></i>', array('class' => 'button small alert template-delete', 'type' => 'button', 'data-id' => $row['id'], 'title' => 'Delete'));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt')->execute()->get('cnt');
		return $data;
	}

	public function get_pages_dropdown($template_id){
		$data = array();
		$s = self::find(array(
			'where' => array(
				'template_id' => $template_id
			),
			'order_by' => array(
				'page' => 'asc'
			)
		));
		if($s) {
			foreach($s as $row) {
				$data[$row->page] = $row->page;
			}
		} else {
			$data[1] = 1;
		}
		return $data;
	}

	public function get_current_page($template_id) {
		$max = \DB::query("SELECT MAX(page) as max_page FROM ".self::$_table_name." WHERE template_id = '{$template_id}'",
				\DB::SELECT)->execute()->current();
		return $max['max_page'];
	}

	public function add_field($data) {
		$response = array();
		$model = self::forge($data);
		$val = $model->validation();
		$val->add_callable('rules');
		$val->add_callable('\Model\Field');
		$val->add_field('pos_x', 'X-Coord', 'numeric_min['.$this->get_min_pos_x().']|numeric_max['.$this->get_max_pos_x().']');
		$val->add_field('pos_y', 'Y-Coord', 'numeric_min['.$this->get_min_pos_y().']|numeric_max['.$this->get_max_pos_y().']');
		if($model->validates()) {
			if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
				$model->pos_x = round($model->pos_x / \Model\Pagesize::MM_IN, 2);
				$model->pos_y = round($model->pos_y / \Model\Pagesize::MM_IN, 2);
				$model->width = round($model->width / \Model\Pagesize::MM_IN, 2);
				$model->height = round($model->height / \Model\Pagesize::MM_IN, 2);
			}
			$model->save(false);
			$response['success']['message'] = '/users/template/edit/'.$data['template_id'].'?page='.$data['page'];
			$response['id'] = $model->id;
		} else {
			$response['errors']['message'] = $val->error_message();
		}
		return $response;
	}

	public function delete_field($id) {
		$field = self::find_by_pk($id);
		if ($field) {
			$field->delete();
			$fields = \DB::select()->from(self::$_table_name)
			->where('page', '=', $field->page)
			->where('template_id', '=', $field->template_id)
			->where('location', '>', $field->location)
			->order_by('location', 'asc')
			->execute()
			->as_array();
			if($fields) {
				$l = $field['location'];
				foreach($fields as $row) {
					\DB::update(self::$_table_name)->value('location', $l)
					->where('id', '=', $row['id'])
					->execute();
					$l++;
				}
			}
		}
	}

	public function save_field($data) {
		$response = array();
		$f = self::find_by_pk($data['id']);
		$f->set($data);
		$val = $f->validation();
		$val->add_callable('rules');
		$val->add_callable('\Model\Field');
		$val->add_field('pos_x', 'X-Coord', 'numeric_min['.$this->get_min_pos_x().']|numeric_max['.$this->get_max_pos_x().']');
		$val->add_field('pos_y', 'Y-Coord', 'numeric_min['.$this->get_min_pos_y().']|numeric_max['.$this->get_max_pos_y().']');
		if($f->validates()) {
			if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
				$f->pos_x = round($f->pos_x / \Model\Pagesize::MM_IN, 2);
				$f->pos_y = round($f->pos_y / \Model\Pagesize::MM_IN, 2);
				$f->width = round($f->width / \Model\Pagesize::MM_IN, 2);
				$f->height = round($f->height / \Model\Pagesize::MM_IN, 2);
			}
			$f->save(false);
			$response['success']['message'] = 'All fields saved';
			$response['id'] = $data['id'];
		} else {
			$response['errors']['message'] = $val->error_message();
		}
		return $response;
	}

	public function field_content($params) {
		$data = \Form::textarea('data[content]', $params['content'], array('id' => 'content'));
		if (in_array($params['type_id'], $this->_t_field_value)) {
			$data = \Form::select('data[content]', null, self::field_content_model($params['type_id']), array('id' => 'content'));
		}
		if (in_array($params['type_id'], $this->_t_image_value)) {
			$data = \Form::input('data[content]', '', array('type' => 'hidden', 'id' => 'content'));
		}
		if ($params['type_id'] == self::T_GFIELD) {
			$data = '<div id="content"></div>';
		}
		return $data;
	}
	public function get_pdf_data($template_id, $page) {
		$query = \DB::select()->from(self::$_table_name)->where('template_id', '=', $template_id);
		if ($page !== null) {
			$query->where('page', '=', $page);
		}
		$query->order_by('page')->order_by('location');
		return $query->execute();
	}

	public static function find_first_placement($template_id, $page_id){
		$query = \DB::select()->from(self::$_table_name)->where('template_id', '=', $template_id)->where('page', '=', $page_id)
			->order_by('location', 'asc')->limit(1)->execute();
		$data = current($query->as_array());
		return $data;
	}

	public function save_placement($data){
		$response = array();
		if (!isset($data['id'])) {
			$row = self::find_first_placement($data['template_id'], $data['page_id']);
			$data['id'] = $row['id'];
		}
		$field = self::find_by_pk($data['id']);
		$field->set($data);
		if ($field === null) {
			$response['error']['message'] = 'Field not found';
			return $response;
		}
		
		$val = $field->validation();
		$val->add_callable('rules');
		$val->add_callable('\Model\Field');
		$val->add_field('pos_x', 'X-Coord', 'numeric_min['.$this->get_min_pos_x().']|numeric_max['.$this->get_max_pos_x().']');
		$val->add_field('pos_y', 'Y-Coord', 'numeric_min['.$this->get_min_pos_y().']|numeric_max['.$this->get_max_pos_y().']');
		if ($field->validates()) {
			if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
				$field->pos_x = round($field->pos_x / \Model\Pagesize::MM_IN, 2);
				$field->pos_y = round($field->pos_y / \Model\Pagesize::MM_IN, 2);
				$field->width = round($field->width / \Model\Pagesize::MM_IN, 2);
				$field->height = round($field->height / \Model\Pagesize::MM_IN, 2);
			}
			$field->save(false);
			$response['success']['message'] = 'Placement saved';

		} else {

			if ((float)$data['pos_x'] < $this->get_min_pos_x()) {
				$field->pos_x = self::MIN_X_COORD;
				if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
					$field->pos_y = round($field->pos_y / \Model\Pagesize::MM_IN, 2);
					$field->width = round($field->width / \Model\Pagesize::MM_IN, 2);
					$field->height = round($field->height / \Model\Pagesize::MM_IN, 2);
				}
				$field->save(false);
			}

			if((float) $data['pos_y'] < $this->get_min_pos_y()) {
				$field->pos_y = self::MIN_Y_COORD;
				if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
					$field->pos_x = round($field->pos_x / \Model\Pagesize::MM_IN, 2);
					$field->width = round($field->width / \Model\Pagesize::MM_IN, 2);
					$field->height = round($field->height / \Model\Pagesize::MM_IN, 2);
				}
				$field->save(false);
			}

			if((float) $data['width'] > self::_max_width($data['pos_x'])) {
				$field->width = (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) ? round(self::_max_width($data['pos_x']) / \Model\Pagesize::MM_IN, 2) : self::_max_width($data['pos_x']);
				if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
					$field->pos_x = round($field->pos_x / \Model\Pagesize::MM_IN, 2);
					$field->pos_y = round($field->pos_y / \Model\Pagesize::MM_IN, 2);
					$field->height = round($field->height / \Model\Pagesize::MM_IN, 2);
				}
				$field->save(false);
			}

			if((float) $data['height'] > self::_max_height($data['pos_y'])) {
				$field->height = (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) ? round(self::_max_height($data['pos_y']) / \Model\Pagesize::MM_IN, 2) : self::_max_height($data['pos_y']);
				if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
					$field->pos_x = round($field->pos_x / \Model\Pagesize::MM_IN, 2);
					$field->pos_y = round($field->pos_y / \Model\Pagesize::MM_IN, 2);
					$field->width = round($field->width / \Model\Pagesize::MM_IN, 2);
				}
				$field->save(false);
			}

			$response['error']['message'] = $val->error_message();
		}
		$response['id'] = $field->id;
		return $response;
	}

	public function delete_page($params){
		$fields = self::find_by(array(
			'template_id' => $params['template_id'],
			'page' => $params['page_id']
		));
		if ($fields !== null) {
			foreach($fields as $row) {
				$row->delete();
			}
		}
		//delete pageimage if any
		\Model\Pageimage::forge()->delete_image($params);
		$this->sequence_page($params['template_id']);
		return true;
	}

	public function sequence_page($template_id){
		$fields = self::find(array(
			'where' => array(
				'template_id' => $template_id
			),
			'order_by' => array(
				'page' => 'asc',
				'location' => 'asc'
			)
		));

		if ($fields !== null) {
			$tmppage = 0;
			$prev = 1;
			$c = 0;
			$seq_page = 1;
			foreach($fields as $i => $row) {
				$seq_page = $row->page;
				if ($row->page != $tmppage) {
					$tmppage = $row->page;
					$c++;
					if (($tmppage - $prev) > 1) {
						$seq_page = $c;
					} else {
						$prev = $row->page;
					}
				}
				$oldpage = $row->page;
				$row->page = $c;
				$row->save(false);
				$params = array(
					'template_id' => $row->template_id,
					'oldpage' => $oldpage,
					'newpage' => $row->page
				);
				\Model\Pageimage::forge()->rename_pageimage($params);
			}
		}
		return true;
	}

}
