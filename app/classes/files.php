<?php
class Files {
	
	const IMG_BACKGROUND_PLACEHOLDER = 'assets/img/template/noimage.jpg';
	
	public static function forge() {
		return new self;
	}
	
	public function load($path){
		$file = glob($path . '.*');
		usort($file, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));
		if(!empty($file) && is_readable(DOCROOT.current($file))) {
			return current($file);
		}
		return false;
	}
	
	public function load_pdf_bg($path){
		$data = array();
		$load = $this->load($path);
		if ($load === false) {
			$data['file'] = self::IMG_BACKGROUND_PLACEHOLDER;
			$data['placeholder'] = true;
		} else {
			$data['file'] = $load;
			$data['placeholder'] = false;
		}
		return $data;
	}
	
}