<?php

class Rules {
	
	/**
	* Validation functions
	*/
	public static function _validation_valid_phone($val) {
		if (trim($val) != '') {
			return (bool)preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/", trim($val));
		} else {
			return true;
		}
	}
	
	public static function _validation_valid_domain($val) {
		if (trim($val) == '') {
			return true;
		}
		return (bool)preg_match('/(?:https?:\/\/)?(?:[a-zA-Z0-9.-]+?\.(?:[a-zA-Z])|\d+\.\d+\.\d+\.\d+)/', trim($val));
	}
	
	public static function _validation_unique_email($val, $model) {
		$model = '\\Model\\'.  ucwords($model);
		$result = $model::find_one_by_email(trim($val), '=');
		return ($result === null) ? true : false;
	}
	
	public static function _validation_unique_email_self($val, $id){
		$result = \Model\User::find_by(array(
			array('id', '!=', $id),
			'email' => $val
		));
		return ($result === null) ? true : false;
	}
	
	public static function _validation_valid_rgb($val){
		return (bool) preg_match("/(#([0-9a-f]{3}){1,2}|(rgba|hsla)\(\d{1,3}%?(,\s?\d{1,3}%?){2},\s?(1|0|0?\.\d+)\)|(rgb|hsl)\(\d{1,3}%?(,\s?\d{1,3}%?){2}\))/i", trim($val));
	}
	
	public static function _validation_unique_title($val){
		$validation = \Validation::active();
		$result = \Model\Group\Field::find_by(array(
			'title' => trim($val),
			'group_id' => $validation->input('group_id')
		));
		return ($result === null) ? true : false;
	}
	
	public static function _validation_unique_title_self($val, $id){
		$validation = \Validation::active();
		$result = \Model\Group\Field::find_by(array(
			array('id', '!=', $id),
			'title' => trim($val),
			'group_id' => $validation->input('group_id')
		));
		return ($result === null) ? true : false;
	}
	
}