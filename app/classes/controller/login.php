<?php
namespace Controller;

class Login extends PublicTemplate {

	public function action_index() {
		$content = \View::forge('login');
		$this->template->meta_title = 'Login';
		$this->template->content = $content;
		$this->template->scripts = array('login.js');
	}

	public function post_auth() {
		return $this->response(\Model\User::forge()->dologin(\Input::post('data')));
	}

	public function action_forgot() {
		$content = \View::forge('forgot_password');
		$this->template->meta_title = 'Forgot Password';
		$this->template->content = $content;
		$this->template->scripts = array('login.js');
	}

	public function post_reset() {
		return $this->response(\Model\User::forge()->reset_password(\Input::post('data')));
	}

	public function action_recover() {
		$user = \Model\User::find_one_by_token($this->param('token'), '=');
		if ($user && $user->status_id == \Model\User::S_PASSWORD) {
			\Authlite::instance('auth_user')->force_login($user->email);
			\Response::redirect('/users/profile');
		}
		\Response::redirect('/');
	}

	public function action_logout() {
		\Authlite::instance('auth_user')->logout();
		\Response::redirect('/login');
	}
}