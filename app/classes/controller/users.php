<?php
namespace Controller;

class Users extends PublicTemplate {
	public function action_index() {
		\Response::redirect('users/dashboard');
	}

	public function get_lists(){
		return $this->response(\Model\User::forge()->all_users(\Input::get()));
	}

	public function action_activate(){
		if (!$this->param('token')) {
			\Response::redirect('/');
		}
		$user = \Model\User::find_one_by_token($this->param('token'), '=');
		if (!$user) {
			\Response::redirect('/');
		}
		$user->status_id = \Model\User::S_ACTIVE;
		$user->token = '';
		$user->save();
		\Authlite::instance('auth_user')->force_login($user->email);
		\Response::redirect('users/dashboard');
	}

	public function post_email_exists(){
		return $this->response(\Model\User::forge()->is_email_used(\Input::post()));
	}

	public function action_invite(){
		if (!$this->param('token')) {
			\Response::redirect('/');
		}
		$user = \Model\User::find_one_by_token($this->param('token'), '=');
		if (!$user) {
			\Response::redirect('/');
		}
		\Authlite::instance('auth_user')->force_login($user->email);
		\Response::redirect('users/profile/?c='.$user->token);
	}

}
