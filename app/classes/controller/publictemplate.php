<?php
namespace Controller;

class PublicTemplate extends \Controller_Hybrid {
	public $template = 'public_base';
	public $member = null;

	public function before() {
		parent::before();
		$this->auto_render = !\Input::is_ajax();

		if ($this->auto_render) {
			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$this->template->content = '';

			$this->template->styles = array();
			$this->template->scripts = array();
		}
		if (!\Input::is_ajax()) {
			$this->template->scripts = array();
		}
	}
	
	public function after($response) {
		return parent::after($response);
	}
}