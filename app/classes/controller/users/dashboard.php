<?php
namespace Controller\Users;

class Dashboard extends Auth {
	public function before() {
		parent::before();
		if($this->user->status_id == \Model\User::S_PASSWORD) {
			\Response::redirect('/users/profile');
		}
	}

	public function action_index() {
		$test = \Model\Template::find_by_pk(1);
		$tables['my_team'] = $tables['my_group'] = '';
		$client['show_assoc'] = false;
		$has_user = \Model\User::find_by_parent_id($this->user->id);
		if($this->user->parent_id == '0' && count($has_user) > 0) {
			$client['show_assoc'] = true;
			$tables['my_team'] = \View::forge('user/my_team');
		}
		if(\Model\Group::find_by_created_by($this->user->id)) {
			$tables['my_group'] = \View::forge('user/my_group');
		}
		$tables['my_client'] = \View::forge('user/my_client', $client);
		$tables['my_template'] = \View::forge('user/my_template');
		$view = \View::forge('user/dashboard');
		$view->view = $tables;
		$this->template->content = $view;
		$this->template->meta_title = 'Trusted - Dashboard';
		$this->template->page_title = 'Dashboard';
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array_merge(array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js', 'jquery.uploadfile.min.js', 'member.js'), $this->template->scripts);
	}

	public function get_lists() {
		$input = \Input::get();
		$input['user_id']	= $this->user->id;
		return $this->response(\Model\User::forge()->all_users($input));
	}

}

