<?php

namespace Controller\Users;

class Template extends Auth {
	
	public function action_index() {
		$js_vars = "";
		$page_id = (\Input::get('page')) ? \Input::get('page') : 1;
		$view = \View::forge('template/index');
		$template = \Model\Template::find_by_pk($this->param('id'));
		if ($this->param('id') && $template && in_array($this->user->id, \Model\User::forge()->get_team_member($template->created_by))) {
			$view->data = $template;
			$js_vars = "var template_id = {$this->param('id')};";
		} else {
			\Response::redirect('/users/dashboard');
		}
		$file = \Files::forge()->load(\Model\Pageimage::IMG_DIR . \Model\Pageimage::IMG_PATH . str_pad($this->param('id'), 8, '0', STR_PAD_LEFT) . '_'.$page_id);
		$js_vars .= ($file) ? "var uploadStr = 1;" : "var uploadStr = 0;";
		$js_vars .= "var MM_IN = ".  \Model\Pagesize::MM_IN .";";
		$view->data['page_dropdown'] = \Form::select('select', $page_id, \Model\Field::forge()->get_pages_dropdown($this->param('id')), array('class' => 'page-dropdown', 'id' => 'page_dropdown'));
		$view->data['page'] = \Model\Field::forge()->total_page($this->param('id'));
		$this->template->js_vars = $js_vars . PHP_EOL;
		$this->template->meta_title = 'User - Template';
		$this->template->page_title = 'Template';
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array_merge(array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js', 'jquery.uploadfile.min.js', 'colors.js', 'jqColorPicker.min.js', 'jquery.uploadfile.min.js', 'raphael.min.js', 'raphael.free_transform.js'), $this->template->scripts);
		$this->template->content = $view;
	}
	
	public function get_lists(){
		$data = \Input::get();
		$data['user_id'] = $this->user->id;
		return $this->response(\Model\Template::forge()->my_templates($data));
	}
	
	public function get_add() {
		$view = \View::forge('dialog/template/form_add_template');
		$view->data = \Model\Template::forge()->get_data();
		return $this->response($view);
	}
	
	public function post_add() {
		$data = \Input::post('data');
		$data['created_by'] = $this->user->id;
		return $this->response(\Model\Template::forge()->update_template($data));
	}
	
	public function post_update() {
		$data = \Input::post('data');
		$data['user_id'] = $this->user->id;
		return $this->response(\Model\Template::forge()->update_template($data));
	}
	
	public function get_fields(){
		return $this->response(\Model\Field::forge()->get_fields(\Input::get()));
	}
	
	public function post_add_page(){
		$input = \Model\Field::forge()->get_data();
		$input['template_id'] = \Input::post('template_id');
		$input['created_by'] = $this->user->id;
		$input['created_by'] = $input['updated_by'] = $this->user->id;
		$input['page'] = \Model\Field::forge()->get_current_page($input['template_id']) + 1;
		if (\Authlite::instance('auth_user')->get_user()->unit == \Model\User::U_INCH) {
			$input['width'] = round($input['width'] * \Model\Pagesize::MM_IN, 2);
			$input['height'] = round($input['height'] * \Model\Pagesize::MM_IN, 2);
			$input['pos_x'] = round($input['pos_x'] * \Model\Pagesize::MM_IN, 2);
			$input['pos_y'] = round($input['pos_y'] * \Model\Pagesize::MM_IN, 2);
		}
		return $this->response(\Model\Field::forge()->add_field($input));
	}
	
	public function get_add_field(){
		$view = \View::forge('dialog/template/form_add_field');
		$view->data = \Model\Field::forge()->get_data();
		$view->data['template_id'] = \Input::get('template_id');
		$view->data['group_id'] = null;
		return $this->response($view);
	}
	
	public function post_add_field(){
		$input = \Input::post('data');
		$input['created_by'] = $input['updated_by'] = $this->user->id;
		return $this->response(\Model\Field::forge()->add_field($input));
	}
	
	public function post_doupload(){
		return $this->response(\Model\Pageimage::forge()->doupload(\Input::post()));
	}
	
	public function get_delete() {
		$view = \View::forge('dialog/template/confirm_delete');
		$view->id = \Input::get('id');
		return $this->response($view);
	}
	
	public function post_delete() {
		return $this->response(\Model\Field::forge()->delete_field(\Input::post('id')));
	}
	
	public function get_edit_field() {
		$view = \View::forge('dialog/template/form_edit_field');
		$view->data = \Model\Field::find_by_pk(\Input::get('id'))->to_array();
		$view->data['user_id'] = $this->user->id;
		$view->data['group_id'] = \Model\Group\Field::forge()->get_group_id($view->data);
		return $this->response($view);
	}
	
	public function post_save_field() {
		return $this->response(\Model\Field::forge()->save_field(\Input::post('data')));
	}
	
	public function get_field_content() {
		$data = \Input::get();
		$data['user_id'] = $this->user->id;
		$data['content'] = $data['group_id'] = null;
		$view = \View::forge('dialog/template/field_content');
		$view->data = $data;
		return $this->response($view);
	}
	
	public function get_image(){
		return $this->response(\Model\Pageimage::forge()->get_image(\Input::get()));
	}
	
	public function get_delete_image(){
		return $this->response(\View::forge('dialog/template/confirm_delete_image'));
	}
	
	public function post_delete_image(){
		return $this->response(\Model\Pageimage::forge()->delete_image(\Input::post()));
	}
	
	public function get_placement(){
		$data['error'] = array();
		if (!\Input::get('id')) {
			$data = \Model\Field::find_first_placement(\Input::get('template_id'), \Input::get('page_id'));
		} else {
			$data = \Model\Field::find_by_pk(\Input::get('id'));
		}
		if($data === null || $data === false) {
			$data['error']['message'] = 'No record found';
		}
		
		return $this->response($data);
	}
	
	public function post_placement(){
		return $this->response(\Model\Field::forge()->save_placement(\Input::post()));
	}
	
	public function get_size(){
		return $this->response(\Model\Pagesize::forge()->get_width_height(\Input::get()));
	}
	
	public function get_is_custom(){
		return $this->response(\Model\Pagesize::forge()->is_custom(\Input::get()));
	}
	
	public function get_delete_page(){
		return $this->response(\View::forge('dialog/template/confirm_delete_page'));
	}
	
	public function post_delete_page(){
		return $this->response(\Model\Field::forge()->delete_page(\Input::post()));
	}
	
	public function get_upload_page(){
		$view = \View::forge('template/uploadbtn');
		$file = \Files::forge()->load(\Model\Pageimage::IMG_DIR . \Model\Pageimage::IMG_PATH . str_pad(\Input::get('template'), 8, '0', STR_PAD_LEFT) . '_'.\Input::get('page'));
		$view->txt = ($file) ? 'Replace Page Image' : 'Upload Page Image';
		return $this->response($view);
	}
	
}
