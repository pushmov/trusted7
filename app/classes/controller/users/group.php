<?php
namespace Controller\Users;

class Group extends Auth {
	
	public function action_index(){
		$view = \View::forge('group/index');
		$view->data = \Model\Group::find_by_pk($this->param('id'));
		$this->template->content = $view;
		$this->template->scripts = array('group.js');
		$this->template->meta_title = 'Edit Group';
		$this->template->page_title = 'Edit Group';
		$this->template->js_vars = "var group_id = {$this->param('id')};". PHP_EOL;
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array_merge(array('datatables.min.js', 'dataTables.responsive.min.js', 'jquery.uploadfile.min.js', 'responsive.foundation.min.js'), $this->template->scripts);
	}
	
	public function get_lists(){
		$params = \Input::get();
		$params['user_id'] = $this->user->id;
		return $this->response(\Model\Group::forge()->all_groups($params));
	}
	
	public function get_add(){
		$view = \View::forge('dialog/group/form_add_group');
		$view->data = \Model\Group::forge()->get_data();
		return $this->response($view);
	}
	
	public function post_add(){
		$data = \Input::post('data');
		$data['created_by'] = $data['updated_by'] = $this->user->id;
		return $this->response(\Model\Group::forge()->update_group($data));
	}
	
	public function get_delete(){
		$view = \View::forge('dialog/group/confirm_delete');
		$view->id = \Input::get('id');
		return $this->response($view);
	}
	
	public function post_delete(){
		return $this->response(\Model\Group::forge()->delete_group(\Input::post('data')));
	}
	
	public function post_update(){
		$data = \Input::post('data');
		$data['updated_by'] = $this->user->id;
		return $this->response(\Model\Group::forge()->update_group($data));
	}
	
	public function get_fields(){
		return $this->response(\Model\Group\Field::forge()->get_lists(\Input::get()));
	}
	
	public function get_field_delete(){
		$view = \View::forge('dialog/group/confirm_delete_field');
		$view->id = \Input::get('id');
		return $this->response($view);
	}
	
	public function post_field_delete(){
		return $this->response(\Model\Group\Field::forge()->delete_field(\Input::post('data')));
	}
	
	public function get_edit_field(){
		$view = \View::forge('dialog/group/form_field');
		$data = (\Input::get('id')) ? \Model\Group\Field::find_by_pk(\Input::get('id')) : \Model\Group\Field::forge()->get_data();
		$view->data = $data;
		return $this->response($view);
	}
	
	public function post_edit_field(){
		$data = \Input::post('data');
		$data['user_id'] = $this->user->id;
		return $this->response(\Model\Group\Field::forge()->update_field($data));
	}
	
	public function get_change_type(){
		$file = \Files::forge()->load(\Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH . str_pad(\Input::get('id'), 8, '0', STR_PAD_LEFT));
		$view = \View::forge('dialog/group/upload_image');
		$view->data = \Input::get('val');
		$view->id = (\Input::get('id')) ? \Input::get('id') : time();
		$view->txt = (\Input::get('id') && $file) ? 'Replace Image' : 'Upload Image';
		return $this->response($view);
	}
	
	public function post_doupload(){
		$output_dir = DOCROOT . \Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH;
		$json = array();
		if (!is_dir($output_dir)){
			mkdir($output_dir, 0755);
		}
		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$filename = str_pad(\Input::post('id'), 8, '0', STR_PAD_LEFT).'.'.$ext;
			if(is_readable(DOCROOT. \Model\Group\Field::IMG_PATH.$filename)){
				unlink(DOCROOT. \Model\Group\Field::IMG_PATH.$filename);
			}
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $filename);
			$json[] = $filename;
		}
		return $this->response($json);
	}
	
	public function get_load_image_type(){
		$response = array();
		$file = \Files::forge()->load(\Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH . str_pad(\Input::get('id'), 8, '0', STR_PAD_LEFT));
		if($file === false) {
			$response['error'] = true;
			$response['image'] = '';
		} else {
			$response['success'] = true;
			$response['image'] = \Model\Group\Field::IMG_DIR . \Model\Group\Field::IMG_PATH . basename($file).'?v='.time();
		}
		return $response;
	}
	
	public function get_group_fields(){
		return $this->response(\Model\Group\Field::forge()->get_fields(\Input::get()));
	}
	
}