<?php
namespace Controller\Users;

class Profile extends Auth {
	public function action_index() {
		$view = \View::forge('user/profile');
		$data['user']	= \Model\User::forge()->profile($this->user->id);
		$data['user']['phone'] = '';
		$data['user']['website'] = '';
		$view->data = $data;
		$this->template->scripts = array('jquery.uploadfile.min.js', 'profile.js');
		$this->template->meta_title = 'User - Profile';
		$this->template->page_title = 'Profile';
		$this->template->js_vars = 'var force_passwrd = false;' . PHP_EOL;
		if ($this->user->status_id == \Model\User::S_PASSWORD) {
			$this->template->js_vars = 'var force_passwrd = true;' . PHP_EOL;
		}
		$this->template->content = $view;
	}

	public function post_doupload(){
		$output_dir = DOCROOT . \Model\User::IMG_DIR . \Model\User::IMG_PATH;
		$json = array();
		if (!is_dir($output_dir)){
			mkdir($output_dir);
		}
		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$filename = str_pad($this->user->id, 8, '0', STR_PAD_LEFT).'.'.$ext;
			if(is_readable(DOCROOT. \Model\User::IMG_PATH.$filename)){
				unlink(DOCROOT. \Model\User::IMG_PATH.$filename);
			}
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $filename);
			$json[] = $filename;
		}
		return $this->response($json);
	}

	public function get_profile_picture() {
		$file = \Files::forge()->load(\Model\User::IMG_DIR . \Model\User::IMG_PATH . str_pad($this->user->id, 8, '0', STR_PAD_LEFT));
		if($file === false) {
			$logo = 'member/default-no-profile-pic.jpg';
		} else {
			$logo = \Model\User::IMG_PATH . basename($file);
		}
		return $this->response(\Asset::img($logo, array('width' => 120, 'height' => 120)));
	}

	public function post_doupdate(){
		$response = array();
		$validated = true;
		$data = \Input::post('data');
		$user = \Model\User::find_by_pk(\Input::post('data.user.id'));
		$user->set(\Input::post('data.user'));
		$val = $user->validation();
		$val->add_callable('rules');

		$validation = \Validation::instance();
		$validation->add_callable('rules');
		$validation->add_field('email', 'Email', 'unique_email_self['.$user->id.']');
		if (!$validation->run(\Input::post('data.user'))) {
			$validated = false;
			$response['error']['message']['user'] = $validation->error_message();
		}

		if (!$user->save()) {
			$validated = false;
			$response['error']['message']['user'] = $val->error_message();
		}
		$data['user']['passwrd'] = $data['user']['cpasswrd'] = $user->passwrd;

		unset($data['user']['cpasswrd']);
		$user->set($data['user']);
		$user->save();
		if (isset($data['team'])) {

			$team->set($data['team']);
			$team->save();
		}
		$response['success'] = array('message' => 'Your profile has beed updated.');
		\Authlite::instance('auth_user')->force_login($user->email);
		return $this->response($response);
	}

	public function get_change_password() {
		return $this->response(\View::forge('dialog/form_change_password', \Input::get()));
	}

	public function post_change_password() {
		$input = \Input::post('data');
		$val = \Validation::instance();
		$val->add_field('passwrd', 'Password', 'required|min_length[6]');
		$val->add_field('cpasswrd', 'Confirm Password', 'required|match_field[passwrd]');
		if (!$val->run($input)) {
			$response['error'] = array('message' => $val->error_message());
			return $this->response($response);
		}
		$user = \Model\User::find_by_pk($this->user->id);
		$user->passwrd = \Authlite::instance('auth_user')->hash($input['passwrd']);
		$user->status_id = \Model\User::S_ACTIVE;
		$user->token = '';
		$user->save(false);
		// Force login the user to update session login info
		\Authlite::instance('auth_user')->force_login($user->email);
		$this->user = \Authlite::instance('auth_user')->get_user();
		$response['success'] = array('message' => 'Password successfully updated.');
		return $this->response($response);
	}

	public function get_update_password() {
		return $this->response(\View::forge('dialog/form_update_password'));
	}

	public function get_create_password(){
		return $this->response(\View::forge('dialog/form_create_password'));
	}

}