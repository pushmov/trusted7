<?php
namespace Controller\Admin;

class Auth extends \Controller_Hybrid {
	public $template = 'admin/admin_base';

	public function before() {
		parent::before();
		$this->auto_render = !\Input::is_ajax();

		// Login check
		if (!\Authlite::instance('auth_sys')->logged_in()) {
			\Response::redirect('/admin/login');
		}

		if ($this->auto_render) {
			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$this->template->content = '';

			$this->template->styles = array();
			$this->template->scripts = array();
		}
	}

	public function after($response) {
		return parent::after($response);
	}
}