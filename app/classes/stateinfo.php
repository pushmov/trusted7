<?php
class StateInfo {
	public static $country = array('CA' => 'Canada','US' => 'United States',);
	public static $state_ca = array(
		'AB' => 'Alberta',
		'BC' => 'British Columbia',
		'MB' => 'Manitoba',
		'NB' => 'New Brunswick',
		'NF' => 'Newfoundland',
		'NT' => 'Northwest Territories',
		'NS' => 'Nova Scotia',
		'ON' => 'Ontario',
		'PE' => 'Prince Edward Island',
		'QC' => 'Quebec',
		'SK' => 'Saskatchewan',
		'YT' => 'Yukon');
	public static $state_us = array(
		'AL' =>    'Alabama',
		'AK' =>    'Alaska',
		'AZ' =>    'Arizona',
		'AR' =>    'Arkansas',
		'CA' =>    'California',
		'CO' =>    'Colorado',
		'CT' =>    'Connecticut',
		'DE' =>    'Delaware',
		'DC' =>    'District of Columbia',
		'FL' =>    'Florida',
		'GA' =>    'Georgia',
		'GU' =>    'Guam',
		'HI' =>    'Hawaii',
		'ID' =>    'Idaho',
		'IL' =>    'Illinois',
		'IN' =>    'Indiana',
		'IA' =>    'Iowa',
		'KS' =>    'Kansas',
		'KY' =>    'Kentucky',
		'LA' =>    'Louisiana',
		'ME' =>    'Maine',
		'MD' =>    'Maryland',
		'MA' =>    'Massachusetts',
		'MI' =>    'Michigan',
		'MN' =>    'Minnesota',
		'MS' =>    'Mississippi',
		'MO' =>    'Missouri',
		'MT' =>    'Montana',
		'NE' =>    'Nebraska',
		'NV' =>    'Nevada',
		'NH' =>    'New Hampshire',
		'NJ' =>    'New Jersey',
		'NM' =>    'New Mexico',
		'NY' =>    'New York',
		'NC' =>    'North Carolina',
		'ND' =>    'North Dakota',
		'OH' =>    'Ohio',
		'OK' =>    'Oklahoma',
		'OR' =>    'Oregon',
		'PW' =>    'Palau',
		'PA' =>    'Pennsylvania',
		'RI' =>    'Rhode Island',
		'SC' =>    'South Carolina',
		'SD' =>    'South Dakota',
		'TN' =>    'Tennessee',
		'TX' =>    'Texas',
		'UT' =>    'Utah',
		'VT' =>    'Vermont',
		'VA' =>    'Virginia',
		'WA' =>    'Washington',
		'WV' =>    'West Virginia',
		'WI' =>    'Wisconsin',
		'WY' =>    'Wyoming');
	/**
	* Return an input field or dropdown based on the state availablity
	*
	* @param string $country
	* @param string $state
	*/
	public static function get_state_form($country, $state = '') {
		$arr = 'state_' . strtolower($country);
		$states = \StateInfo::$$arr;
		if ($states === NULL) {
			return '<input type="text" name="data[state]" value="' . $state . '" />';
		} else {
			return \Form::select('data[state]', $state, $states, array('id' => 'state'));
		}
	}
}