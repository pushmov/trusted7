<?php

class Emails {
	protected $_email;
	protected $_bcc;
	public function __construct() {
		\Package::load('email');
		$this->_email = \Email::forge();
		$this->_bcc = 'tester@tester.com';
	}
	
	public static function forge() {
		return new self;
	}
	
	public function send() {
		if (($bcc = $this->_bcc) != '') {
			$this->_email->bcc($bcc);
		}
		try {
			$this->_email->send();
			$e_response = array('success' => array('message' => 'Email sent'));
		} catch(\EmailValidationFailedException $e) {
			$e_response = array('error' => array('message' => $e->getMessage()));
		} catch(\EmailSendingFailedException $e) {
			$e_response = array('error' => array('message' => $e->getMessage()));
		}
		return $e_response;
	}
	
	public function welcome($data) {
		$this->_email->to($data['user']['email']);
		$this->_email->subject('Welcome to Trusted!');
		$this->_email->html_body(\View::forge('email/welcome', $data));
		return $this->send();
	}
	
	public function reset_password($data) {
		$this->_email->to($data['email']);
		$this->_email->subject('Password reset instructions');
		$this->_email->html_body(\View::forge('email/reset_password', $data));
		return $this->send();
	}
	
	public function invite_user($data) {
		$this->_email->to($data['email']);
		$this->_email->subject('Invitation to Join Trusted');
		$this->_email->html_body(\View::forge('email/invite_user', $data));
		return $this->send();
	}
	
	public function client_send_message($data) {
		$this->_email->to($data['email']);
		$this->_email->subject($data['subject']);
		$this->_email->html_body(\View::forge('email/client_send_email', $data));
		return $this->send();
	}
	
}